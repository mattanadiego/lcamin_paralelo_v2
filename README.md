## PARALLEL LCAOMIN 

1. Library instalation 

```bash
sudo apt-get install mpich
sudo apt-get install octave
sudo apt-get install liboctave-dev
```

2. Paths configuration

    1. Change in Makefile the octave installed version:
    
    ```bash
    OCTAVE_VERSION := 4.0.0
    ```

    2. Change in Makefile the paths relative to the libraries installed:
    
    ```bash
    OCTAVE_INCLUDE_DIR :=/usr/include/octave-$(OCTAVE_VERSION)/
    MPI_INCLUDE_DIR :=/usr/include/mpich
    LCAOMIN_SRC_DIR :=/home/laura/lcaomin_jor_paralelo
    ```

    3. Change the paths in agua.m and globales.m (To be modified like lcaomin_sequential)
    
    ```C
    addpath('/home/laura/lcaomin_jor_paralelo','/home/laura/lcaomin_jor_paralelo/ejemplos')
    ```