
% Funcion integrada en cs_3, utilizada en calc_s

function [yyr,yyi]=fcs_3(x,alfa,beta,norma)
   yyr=((x.^3) ./ ((4*beta*x.^2+alfa.^2).^1.5)) .*...
      exp(-x.^2-(beta*(alfa.^2)*((norma)./(4*beta*x.^2+alfa.^2))));
   yyi=[];
return
