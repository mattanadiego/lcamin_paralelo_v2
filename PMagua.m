function [pinicial,mum,chgd,indp,corrida]=pmagua
% jorge ast. corrida='2'
global objetivo
objetivo='agua';
corrida=4

%pinicial=[0.7 0.9];
%pinicial=[0.79425742550393   	1.09539243051783];
%pinicial=[0.81982040725641   	1.10225630154408];
%pinicial=[0.82250188127395   	1.10650232294715];
%pinicial=[0.82382742926509   1.11059266877258];
%pinicial=[0.82455853468586   1.11443063875320];
%pinicial=[0.82531507900243   1.11805799578961];
%pinicial=[0.82593469782704   1.12152237802559];
%pinicial=[0.82635586688362   1.12466796335661];
%pinicial=[0.82680502046877   1.12763075950713];
%pinicial=[0.82737900321885   	1.13045645780194]; 
%pinicial=[0.82777068646675   1.13303186425539];
%pinicial=[0.82815980940996   	1.13537205975939];
%pinicial=[0.82880552480745   1.13974528105902];
%pinicial=[0.82909743259060   1.14169816594341];
%pinicial=[0.9   1.14169816594341];
%pinicial=[0.82794536775765    1.15146711224393];  
%pinicial=[0.83004208662760   1.15243811354078];
%pinicial=[0.83060474338980   1.15352941326342];
%pinicial=[1.9 54];
%pinicial=[1.84417365541148  53.99996860694193];
%pinicial=[1.83939995005263  54.00004018087790];
pinicial=[1.84026582344112  54.00012287539607];

mum=[1:25];

%En la matriz mum se indican, por filas, los
%orbitales que afecta cada par�metro. Si 
%mum=[3 6; 5 0] resulta que el parametro 1 afecta
%a los orbitales 3 y 6 y el parametro 2 solo al
%orbital 5. 
% Parametros: 
%	  13  14  10  11  1   2   3
%mum=[5 7 0 0;6 8 0 0;3 0 0 0;4 0 0 0;5 6 7 8;2 4 0 0;1 0 0 0;2 0 0 0];
%mum =[
%     5     7     0     0;
%     6     8     0     0;
%     3     0     0     0;
%     4     0     0     0;
%     5     6     7     8;
%     2     4     0     0;
%     1     0     0     0;
%     2     0     0     0;]
%Energ�a final: -75.37092429553897
%pfinal=[2.74936044277093   0.50319455127418]

chgd=[1e-3 1e-3];

%
% 		Indicar en chgd los incrementos para usar 
%		en el c�lculo de las derivadas parciales
% 		respecto de cada par�metro en pinicial
%

%
% 		Indicar en indp los par�metros de pinicial 
%		que deben mantenerse positivos
%
indp=1:length(pinicial);
%
if ~(ischar(corrida))
   corrida='000';
end

return
 