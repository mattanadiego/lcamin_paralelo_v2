% Funcion integrada en cv_2, utilizada en calc_va

function [yyr,yyi]=fcv_2(fi,u,er,a,alfa,b,beta,r)
	rmut=a-r;
	rnut=b-r;
   h1=exp(-alfa*(er.^2-2*er.*(1-u^2).^(0.5).*(rmut(1)*cos(fi)+...
      rmut(2)*sin(fi))-2*er.* u* rmut(3) + rmut'*rmut).^.5);
   %jorge en acci�n h2=exp(-beta*(er.^2-2*er.*(1-u^2)^.5.*(rnut(1)*cos(fi)+...
   h2=exp(-beta*(er.^2-2*er.*(1-u^2).^(0.5).*(rnut(1)*cos(fi)+...
      rnut(2)*sin(fi))-2*er.* u* rnut(3) + rnut'*rnut).^(0.5));
   yyr = h1 .* h2.* er;
   yyi=[];
return
