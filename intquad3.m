% Procedimiento para realizar la integración triple.
function [fx]=intquad3(F,xl,yl,zl,puntos,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20 intq24
global intq32 intq40 intq60 intq80 intq100 intq120 intrec
switch puntos 
case 2
   intq=intq2;
case 3
   intq=intq3;
case 4
   intq=intq4;
case 6
   intq=intq6;
case 8
   intq=intq8;
case 12
   intq=intq12;
case 16
   intq=intq16;
case 20
   intq=intq20;
case 24
   intq=intq24;
case 32
   intq=intq32;
case 40
   intq=intq40;
case 60
   intq=intq60;
case 80
   intq=intq80;
case 100
   intq=intq100;
case 120
   intq=intq120;
otherwise
   intq=intq12;
end

% check for complex input */
if ~isreal(xl)
   %errorlog "ERROR: Not implemented for complex matrices.";
else
   xl = real(xl);
end

if ~isreal(yl)
   %errorlog "ERROR: Not implemented for complex matrices.";
else
   yl = real(yl);
end

if ~isreal(zl)
   %errorlog "ERROR: Not implemented for complex matrices.";
else
   zl = real(zl);
end

if intrec ==0
   if size(xl,1) ~= 2
      %errorlog "ERROR:  X limit vector must have 2 rows";
   elseif size(yl,1) ~= 2
      %errorlog "ERROR:  X limit vector must have 2 rows";
   elseif size(zl,1) ~= 2
      %errorlog "ERROR:  X limit vector must have 2 rows";
	else
      chk = [size(xl,2);size(yl,2);size(zl,2)];
      n = max(chk);
      if ~(all(chk == 1) | all(chk == n)) 
         %errorlog "ERROR:  Limit matrices are not conformable.\r\l";
      else
         xl = zeros(2,n) + xl;
         yl = zeros(2,n) + yl;
         zl = zeros(2,n) + zl;
      end
   end
end
e = intq(:,1);
w = intq(:,2);
if intrec <=1
   if intrec ==0
      diff = xl(2,:)' - xl(1,:)';
      intrec = 1;
      xc = 0.5*((xl(1,:)'+xl(2,:)')*ones(1,length(e))+(diff * e'));
      fx = intquad3(F,xc,yl,zl,puntos,varargin{:});
      fx = ((diff/2).* (fx*w));
      intrec = 0;
      return
   else
      intrec = 2;
      xc = xl; 
      diff = yl(2,:)' - yl(1,:)';
      yc = 0.5*( (yl(1,:)'+yl(2,:)')*ones(1,length(e))+...
         (diff * e'));
      fx = xc;
      for ii=1:size(yc,2)
         [t] = intquad3(F,xc(:,ii),yc,zl,puntos,varargin{:});
         fx(:,ii) = ((diff/2).*(t*w));
      end
   end
else
   xc = xl;
   yc = yl;
   diff = zl(2,:)' - zl(1,:)';
   zc = 0.5*((zl(1,:)'+zl(2,:)')*ones(1,length(e))+...
      ( diff * e'));
   fx = yc;   
   for ii=1:size(yc,2)
      t = zeros(size(zc,1),size(zc,2))+...
          feval(F,xc*ones(1,size(yc,2)),yc(:,ii),zc,varargin{:});
      fx(:,ii) = ((diff/2).*(t*w));
   end
end
return


