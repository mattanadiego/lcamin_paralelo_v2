% Funcion que se integra en la rutina m11_22.
function [fu]=fu11_22(parxy,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
u1=parxy(:,1);
u2=parxy(:,2);
[t,sxy,rsxy]=te11_22(u1,u2,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
f0=efe0(t);
ra1a2=(a1-a2)'*(a1-a2);
d1=(alfa1^2*alfa2^2)./(4*sxy);
num=exp(-u1.^2-u2.^2);
num=num.*(u1.^3).*(u2.^3);
num=num.*exp(-d1*ra1a2);
den=sxy.*sqrt(rsxy);
fu=num./den;
fu=(fu.*f0)';
return

% ================================================
function [s2]=suma2(u1,u2,alfa1,alfa2);
	s2=alfa1^2*u2.^2+alfa2^2*u1.^2;
return

% ================================================
% Procedimiento que genera la combinacion convexa*/
function [y]=pconv2(u1,u2,a1,alfa1,a2,alfa2)
	y=zeros(size(u2,1),3);
  	y(:,1)=alfa1^2*(u2).^2*a1(1)+alfa2^2*(u1).^2*a2(1);
  	y(:,2)=alfa1^2*(u2).^2*a1(2)+alfa2^2*(u1).^2*a2(2);
  	y(:,3)=alfa1^2*(u2).^2*a1(3)+alfa2^2*(u1).^2*a2(3);
  	yd=suma2(u1,u2,alfa1,alfa2);
  	y=y./(yd*ones(1,3));
return

% ================================================
% Procedimiento que genera el t de la funcion f0
function [t,t1,tt1]=te11_22(u1,u2,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
	t1=suma2(u1,u2,alfa1,alfa2);	
  	tt1=t1+4*(beta1+beta2)*(u1.^2).*(u2.^2);
  	y1=pconv2(u1,u2,a1,alfa1,a2,alfa2);
 	y2=(beta1*b1'+beta2*b2')/(beta1+beta2);
  	yy=y1-ones(size(y1,1),1)*y2;
  	norma=sum((yy.*yy)')';
  	t=t1.*(beta1+beta2)./tt1;
  	t=t.*norma;
return

