% Cuatro funciones de m�dulo 1. (mu mu|nu nu)
% ------------------------------------------------------   
function [fxr]=m1b_m1b(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)  
psedaa=0.5*(alfa1+alfa2);
psedab=0.5*(beta1+beta2);
pseda =0.5*(psedaa+psedab);
norma=norm(a1-b1);
ro=pseda*norma;
tau=((alfa1+alfa2)-(beta1+beta2))/((alfa1+alfa2)+(beta1+beta2));
taua=(alfa1-alfa2)/(alfa1+alfa2);
taub=(beta1-beta2)/(beta1+beta2);
if ro 		% Centros diferentes 
   if tau	% Promedios de alfas diferentes
      roa=(1+tau)*ro;
      rob=(1-tau)*ro;
      kapa=0.5*(tau+(1/tau));
      fxr=(pseda/ro)*(1-(1-kapa)^2*(0.25*(2+kapa)+0.25*roa)*...
         exp(-2*roa)-(1+kapa)^2*(0.25*(2-kapa)+0.25*rob)*exp(-2*rob));
   else
      fxr=(pseda/ro)*(1-(1+(11/8)*ro+(3/4)*ro^2+(1/6)*ro^3)...
         *exp(-2*ro));
   end
else			% Centros coincidentes
      fxr=(1/8)*(1-tau^2)*(5-tau^2)*pseda;
end
fxr=(((1+taua)*(1-taua)*(1+taub)*(1-taub))^1.5)*fxr;
 
return


function [fxr]=m1b_m1bold(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)  
psi=0.5*(alfa1+beta1);
norma=norm(a1-b1);
ro=psi*norma;
if ~(alfa1==beta1)
   % tau no nulo
   if ro
      rou=alfa1*norma;
      ron=beta1*norma;
      ctek= (alfa1^2 + beta1^2)/(alfa1^2-beta1^2);
      fxr=(psi/ro)*(1-(1-ctek)^2*(0.25*(2+ctek)+0.25*rou)*...
         exp(-2*rou)-(1+ctek)^2*(0.25*(2-ctek)+0.25*ron)*exp(-2*ron));
      
   else
      % ro nulo
      tau=(alfa1-beta1)/(alfa1+beta1);
      fxr=(1/8)*(1-tau^2)*(5-tau^2)*psi;
   end
else
   if ro
      % tau=0, ro no nulo      
      fxr=(psi/ro)*(1-(1+(11/8)*ro+(3/4)*ro^2+(1/6)*ro^3)...
         *exp(-2*ro));
   else
      % tau=0, ro=0   
      fxr=(5/8)*alfa1;
   end
end
return

  
function [fxr]=m1b_m1bnew(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)  
psedaa=0.5*(alfa1+alfa2);
psedab=0.5*(beta1+beta2);
pseda =0.5*(psedaa+psedab);
norma=norm(a1-b1);
ro=pseda*norma;
tau=((alfa1+alfa2)-(beta1+beta2))/((alfa1+alfa2)+(beta1+beta2));
if ro 		% Centros diferentes 
   if tau	% Promedios de alfas diferentes
      taua=(1+tau)*ro;
      taub=(1-tau)*ro;
      kapa=0.5*(tau+1)/tau;
      fxr=(pseda/ro)*(1-(1-kapa)^2*(0.25*(2+kapa)+0.25*taua)*...
         exp(-2*taua)-(1+kapa)^2*(0.25*(2-kapa)+0.25*taub)*exp(-2*taub));
      fxr=(((1+taua)*(1-taua)*(1+taub)*(1-taub))^1.5)*fxr;
   else
      taua=(1+tau)*ro;
      taub=(1-tau)*ro;
      fxr=(pseda/ro)*(1-(1+(11/8)*ro+(3/4)*ro^2+(1/6)*ro^3)...
         *exp(-2*ro));
      fxr=(((1+taua)*(1-taua)*(1+taub)*(1-taub))^1.5)*fxr;
   end
else			% Centros coincidentes
   if tau	% Promedios de alfas diferentes
      fxr=(1/8)*(1-tau^2)*(5-tau^2)*pseda;
   else
      % tau=0, ro=0   
      fxr=(5/8)*pseda;
   end
end
return

