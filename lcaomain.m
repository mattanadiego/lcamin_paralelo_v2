function [md,lcaomain_time] = lcaomain(my_rank, nproc, mdatos, param0, gcnt)
%function [energy,autoval,md,cs,pse,s,ctr]=agua(my_rank,varargin)

%disp('Comienza lcaomain')
lcaomain_time_tic = tic;

% global mdatos comp_4in_time comp_v_time lcaomain_time mol_total_energy;
global lcaomain_path %param_c_size param0 gcnt

clc

%if ~nargin 
%   clear all

lcaomin_path = getenv("LCAOMINPATH");
ejemplospath = strcat(lcaomin_path, 'agua/');
%disp(ejemplospath);
addpath(lcaomin_path, ejemplospath);
%   limpia
param0 = [];
gcnt = [];

%end
%if nargin==1
%   gcnt=[];
%end

global mum

% *************************************************************
% 		ENCABEZAMIENTO Y VARIABLES GLOBALES. NO MODIFICAR.
% *************************************************************

global orden puntosx puntosr
orden = 0;
puntosx = 20;
puntosr = 20;
globales
% *************************************************************
% 	      DATOS A MODIFICAR CON CADA EJEMPLO
% *************************************************************

% Identificación del ejemplo
nomarch = strcat('a',mdatos);
optim = 1;
%Valores de los z. La suma debe ser par!!
[md,centros,ldiv] = feval(mdatos,param0);

clc
%save mat_md_input md
save centros centros

%============================ nueva modificacion 20/10 =============
%function [energy,autoval,md,cs,pse,s,ctr]=lcao(md,centros,ldiv,optim)

% Variables globales.
global nomarch %lcaomain_path mdatos 
   
% L�mites
clc
clsmd = size(md, 2);
md = get_dat(md);
sz = 0.5 * sum(centros(4,:)); %Semisuma de zhh
%sz=5; %8/2/7

sqp1 = sqp(clsmd); 

index_par_dos = index_par_dos(sqp1);
%%%%%%%%%%%%%%%%%%
%=====================================================================
save index_par_dos index_par_dos
savemat('md', 3, md); 
savemat('ldiv', 0, ldiv);
sqp1 = INDICES2(clsmd);
save sqp1 sqp1 
partirSqp1(sqp1,nproc);

%save ldiv ldiv 
%save sqp1 sqp1 
%save orden orden 

%%%%%%%%%%%%%%%%%%Particionar spq1, guardarlas a disco y que master las env�e a workers


%disp('soy el procesador: ')
%disp(my_rank)

%disp('size: ')
%disp(nproc)

%if isempty(gcnt)
%   tic
%   [energy,autoval,md,cs,pse,s,ctr]=lcao(md,centros,ldiv,optim);
%   toc
%else
%   tic
%   [energy,autoval,md,cs,pse,s,ctr]=lcao(md,centros,ldiv,optim);
%   toc
%end

lcaomain_time = toc(lcaomain_time_tic);
save lcaomain_time lcaomain_time
%disp('Finaliza lcaomain')

return


