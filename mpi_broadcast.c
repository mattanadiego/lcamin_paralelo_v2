#include <mpi.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comunes.c" //TODO: pasar a comunes.h

#define PORCENTAJE_HARD 60
#define PORCENTAJE_MEDDIUM 30
//se pone como limite 10 caracteres por fila (en caso de 100 funciones la fila puede llegar a ser " 100 100 ")
#define MAX_CANT_CARACTERES_FILA 10
#define DEBBUG 0
#define TAG_ALL 99


/*
    Actualiza los argumentos paresHard, paresMeddium, paresSoft con las porciones de matriz de cada grupo.
    El parametro cantidad_filas es la longitud de matriz en filas (a nivel texto, es decir contando los enters).  
*/
void particionar_pares(char * matriz, int cantidad_filas, char ** paresHard, char ** paresMeddium, char ** paresSoft, int * filasHard, int * filasMeddium, int * filasSoft){

    * filasHard    = cantidad_filas/3;
    * filasMeddium = cantidad_filas/3;
    * filasSoft    = cantidad_filas - *filasHard - *filasMeddium ; //el resto de filas

    int count_enter  = 0, 
        i=0,j=0;

    *paresHard    = (char * ) malloc(sizeof(char)* (*filasHard)* MAX_CANT_CARACTERES_FILA);
    *paresMeddium = (char * ) malloc(sizeof(char)* (*filasMeddium)* MAX_CANT_CARACTERES_FILA);
    *paresSoft    = (char * ) malloc(sizeof(char)* (*filasSoft) * MAX_CANT_CARACTERES_FILA);

    //creación del primer tramo
    for(i=0;count_enter< *filasHard;i++){
        (*paresHard)[i]=matriz[i];
        //en line feed cuento una fila más.
        if(matriz[i]==10)
            count_enter++;
    }    
    (*paresHard)[i] = 0;
    
    //segundo tramo
    count_enter=0;
    for(j=0;count_enter<*filasMeddium;j++,i++){
        //el indice i queda avanzado,
        //el indice j controla el acceso al tramo.
        (*paresMeddium)[j]=matriz[i];
        //en line feed cuento una fila más.
        if(matriz[i]==10)
            count_enter++;
    }    
    (*paresMeddium)[j] = 0;
    
    //tercer tramo  
    count_enter=0;
    for(j=0;count_enter<*filasSoft && i<strlen(matriz);j++,i++){
        //el indice i queda avanzado,
        //el indice j controla el acceso al tramo.
        (*paresSoft)[j]=matriz[i];
    
        //en line feed cuento una fila más.
        if(matriz[i]==10){
            count_enter++;
        }
    }    
    
    (*paresSoft)[j] = 0;    
}

/*
  Esta función va a devolver la porción de pares i,j del subgrupo (parametro pares) que le corresponde a hijo
  de acuerdo al tamaño de la partición.
  Offset_tramo indica por donde empezar a copiar.
  Además como parámetro resultado se devuelve el tamaño de la cadena devuelta. 
*/
void obtener_tramo_hijo(char * pares,int cantidad_filas, int offset_tramo, int nproc_particion, int * longitud_tramo,char * tramo){
    int longitud = strlen(pares)/nproc_particion; //longitud en bytes
    int filas_tramo = cantidad_filas/nproc_particion;

    //char * tramo = (char * ) malloc(sizeof(char)*filas_tramo*16); //filas de tramo*16 caracteres máximo por fila    
    int i =0;
    int j ;
    int count_filas =0, count_filas_aux=0;

    if(tramo==0){
        printf("ERROR !!! no hay mas memoria\n");   
        exit(-1);
    }
    //para determinar la posicion incial en pares debo avanzar el j hasta haber encontrado la suficiente cantidad de filas
    for(j=0;count_filas_aux<(offset_tramo-1)*filas_tramo;j++){
        if (pares[j]==10)
            count_filas_aux++;
       
    }
    if(DEBBUG) printf("j=%d longitud=%d cantidad_filas=%d filas_tramo=%d offset_tramo %d \n\n",j,longitud,cantidad_filas, filas_tramo,offset_tramo);
    
    //copio el respectivo tramo.
    for(i=0,j;count_filas<filas_tramo;i++,j++){
        // printf(" tramo[%d]=%c",i,pares[i]);
        //i comienza siempre de cero, j va variando de acuerdo al offset calculado anteriormente.
        tramo[i] = pares[j]; 
        if (pares[j]==10)
            count_filas++;

    }
    tramo[i]=0;
    *longitud_tramo=i+1;
    //return tramo;
}

/*Este metodo lo ejecuta solo el padre (rank==0)*/
int mpi_broadcast(int nproc)
{
	/*levantar las matrices*/
  int fd_ldiv,fd_sqp1,fd_orden,fd_index_par_dos;
    const char * md_name="md", 
               * ldiv_name="ldiv",
               * sqp1_name="sqp1",
               * centros_name="centros";
       
    long md_size;   //para guardar los tamaños de las matrices (bytes leidos del archivo en disco)
    long ldiv_size;
    long centros_size;
        
    char * md = load_matrix(md_name,&md_size);
    char * ldiv = load_matrix(ldiv_name, &ldiv_size);
    //char * sqp1 = load_matrix(sqp1_name, &sqp1_size);    /*la matriz sqp1 contiene todas las funciones a calcular*/
    char * centros = load_matrix(centros_name, &centros_size);    /*la matriz sqp1 contiene todas las funciones a calcular*/

    //char * sqp1_sinEncabezado; /*Para almacenar sqp1 sacando el encabezado que genera octave*/

    //char * paresHard, * paresMeddium, * paresSoft;

    /*Las siguientes variables corresponden a la porción de procesadores que calculan cada parte. 
      Para el calculo no se considera el procesador padre.*/
    int HARD    = PORCENTAJE_HARD*(nproc-1)/100;
    int MEDDIUM = PORCENTAJE_MEDDIUM*(nproc-1)/100;
    int SOFT    = (nproc-1)-HARD-MEDDIUM;

    int cantidad_filas; //para contar las filas de sqp1
    /*Para que ninguno de los grupos sea vacío hay que ejecutar con nproc>=3.*/
    if(MEDDIUM ==0 || SOFT == 0){
        printf("El grupo de procesadores MEDDIUM o SOFT es vacío. Imposible particionar.");
        free(md);
        free(ldiv);
        //free(sqp1);
        exit(-1);
    }
    

    /***************************************************************************************/
    /*  Procesamiento de SQP1: construcción de los tramos para cada hijo                   */
    /***************************************************************************************/
    int filasHard;
    int filasMeddium;
    int filasSoft;

    //sqp1_sinEncabezado= remover_encabezado_matriz(sqp1);
    //cantidad_filas = contar_filas(sqp1_sinEncabezado);//TODO: cambiar por obtener size desde matlab:ineficiente

    //printf("\nSQP1 es: %s longitud=%d cantidad filas=%d\n",sqp1_sinEncabezado,strlen(sqp1_sinEncabezado),cantidad_filas);

    //particionar_pares(sqp1_sinEncabezado, cantidad_filas, &paresHard, &paresMeddium, &paresSoft,&filasHard,&filasMeddium,&filasSoft);
    
    /*if(DEBBUG) printf("HARD= %d procesadores, MEDDIUM= %d procesadores, SOFT=%d procesadores\n", HARD, MEDDIUM, SOFT);
    if(DEBBUG) printf("\nPRIMER TRAMO (tamano %d): \n %s \n",strlen(paresHard),paresHard);
    if(DEBBUG) printf("\nSECONDO TRAMO (tamano %d): \n %s \n",strlen(paresMeddium),paresMeddium);
    if(DEBBUG) printf("\nTercer TRAMO (tamano %d): \n %s \n",strlen(paresSoft),paresSoft);*/
    
	int hijo=1;
      
    char * tramo;  //servirá para ir calculando cada tramo correspondiente a cada hijo, en la porción de pares correspondiente
    int longitud_tramo; 

    //necesito restar el caracter de fin de archivo.
    filasSoft--;

    printf("SQP1 particionado! Enviado por tramos...\n");
    void * tramo1=0;
    char sqp1_i[8];
    
    /*Envío al grupo de hijos dedicado a HARD una parte de funciones HARD.*/
//	for(hijo;hijo<nproc;hijo++){

        //int filas_tramo = cantidad_filas/HARD;
        //tramo1=malloc(filas_tramo*5); //filas de tramo*16 caracteres máximo por fila    
//         long sqp1_i_size;

//	    sprintf(sqp1_i,"sqp1_%d",hijo);
//	    sqp1_i[strlen(sqp1_i)+1]='\0';

//        char * mat_sqp1_i = load_matrix(sqp1_i,&sqp1_i_size);

        //obtener_tramo_hijo(paresHard,filasHard, hijo, HARD, &longitud_tramo,tramo);    
   

  //      MPI_Send(mat_sqp1_i,sqp1_i_size,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
       
//        if (DEBBUG) printf("Padre: enviado a %d tramo %s longitud_tramo %d \n",hijo,tramo, longitud_tramo);
//        printf("LLEGO abri la matriz %s size sqp1 = %ld  \n",sqp1_i, sqp1_i_size);
//        getchar();
  
         //TODO arreglar esto.
//        free(mat_sqp1_i);
//	}

/*	for(hijo;hijo<=MEDDIUM+HARD;hijo++){
        tramo = obtener_tramo_hijo(paresMeddium, filasMeddium,hijo- HARD, MEDDIUM, &longitud_tramo);    
        MPI_Send(tramo,longitud_tramo,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
        if (DEBBUG) printf("Padre: enviado a %d tramo %s longitud_tramo %d \n",hijo,tramo, longitud_tramo);
        free(tramo);
	}

	for(hijo;hijo<nproc;hijo++){
        tramo = obtener_tramo_hijo(paresSoft, filasSoft, hijo-HARD-MEDDIUM, SOFT, &longitud_tramo);    
        MPI_Send(tramo,longitud_tramo,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
        if (DEBBUG) printf("Padre: enviado a %d tramo %s longitud_tramo %d \n",hijo,tramo, longitud_tramo);
        free(tramo);

	}
*/

    /***************************************************************************************/
    /*  Envío la matriz de datos a todos los hijos.                                        */
    /***************************************************************************************/
    hijo=1;
    for(hijo;hijo<nproc;hijo++){        
        MPI_Send(md,md_size,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
        //printf("Padre: enviado MD a %d enviados: %ld \n",hijo,md_size);
    }
  
    /***************************************************************************************/
    /*  Envío los centros orbitales: TODO ver si hace falta enviarlos o se pueden calcular en octave a partir de md */
    /***************************************************************************************/
    hijo=1;
    for(hijo;hijo<nproc;hijo++){        
        MPI_Send(centros,centros_size,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
        //printf("Padre: enviado CENTROS a %d enviados: %ld \n",hijo,centros_size);       
    }

    /***************************************************************************************/
    /*  Envío de ldiv: TODO ver si hace falta enviarlo o se pueden calcular en octave a partir de md */
    /***************************************************************************************/
    hijo=1;
    for(hijo;hijo<nproc;hijo++){        
        MPI_Send(ldiv,ldiv_size,MPI_UNSIGNED_CHAR, hijo, TAG_ALL, MPI_COMM_WORLD);
        //printf("Padre: enviado LDIV a %d enviados: %ld \n",hijo,ldiv_size);       
    }


    if(md)
        free(md);
    
    if(ldiv)
        free(ldiv);

    /*if(sqp1!=0)
        free(sqp1);*/

/*    if(paresHard!=0)
       free(paresHard);

    if(paresMeddium !=0)
        free(paresMeddium);

    if(paresSoft!=0)
        free(paresSoft);*/

	printf("Broadcast Completado!\n");
	return 0;
}


