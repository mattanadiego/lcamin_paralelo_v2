% Funcion que se integra en la rutina m12_22.
function [fu]=fu12_22(u,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
	[t,sx,sxy]=te12_22(u',a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   f0=efe0(t);
   ra1a2=(a1-a2)'*(a1-a2);
   d1=(alfa1^2*alfa2)./sx;
   num=exp(-u'.^2).*u'.^3;
   num=num.*exp(-d1*ra1a2);
   den=sx.*sqrt(sxy);
   fu=num./den;
   fu=(fu.*f0)';
return

% ================================================
% Procedimiento que genera la combinacion convexa*/
function [y]=pconv3(u,a1,alfa1,a2,alfa2)
	y=zeros(size(u,1),3);
  	y(:,1)=alfa1^2*a1(1)*ones(size(u,1),1)+4*alfa2*u.^2*a2(1);
  	y(:,2)=alfa1^2*a1(2)*ones(size(u,1),1)+4*alfa2*u.^2*a2(2);
  	y(:,3)=alfa1^2*a1(3)*ones(size(u,1),1)+4*alfa2*u.^2*a2(3);
  	yd=alfa1^2+4*u.^2*alfa2;
  	y=y./(yd*ones(1,3));
return

% ================================================
% Procedimiento que genera el t de la funcion f0
function [t,t1,tt1]=te12_22(u,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
	t1=alfa1^2+4*u.^2*alfa2;
  	tt1=t1+4.*u.^2*(beta1+beta2);
	t=t1*(beta1+beta2)./tt1;  
  	y1=pconv3(u,a1,alfa1,a2,alfa2);
 	y2=(beta1*b1'+beta2*b2')/(beta1+beta2);
  	yy=y1-ones(size(y1,1),1)*y2;
  	norma=sum((yy.*yy)')';
  	t=t.*norma;
return

