function [comp_4in_time]=lcao(md,centros,sqp1,optim,my_rank,num_seed,mdatos, num_procs)

% *************************************************************
%				RUTINAS DE CALCULO. NO MODIFICAR!!
% *************************************************************

% Variables globales.
%global nomarch lcao_path mdatos 
   
% L�mites
%clc
%clsmd=size(md,2);
%md = get_dat(md);
%sz=0.5*sum(centros(4,:)); %Semisuma de zhh
%sz=5; %8/2/7
%sqp1=sqp(clsmd);
%tic
%[vt,vs,vv]=comp_v(md,centros,ldiv,sqp1);
%toc
%disp('Finalizo comp_v')
%%%%%%%%%%%%%%%%%%
%tic

%LAURA: aca hay que hacer el load de md, ldiv y sqp1 de nuevo. 
%LAURA: esto va a ser invocado en cada nodo, con lo cual las matrices

data_path = getenv("LCAOMINPATH");
data_path = strcat(data_path, 'agua/');

time_file = strcat(data_path, mdatos);
time_file = strcat(time_file, '_');
procs_s = num2str(num_procs);
time_file = strcat(time_file, procs_s);
time_file = strcat(time_file, 'procs_time_proc');
rank_s = num2str(my_rank);
time_file = strcat(time_file, rank_s);

%[vclmn,mat1,mat2,indices]=comp_4in(md,ldiv,sqp1);
%disp('Comienza el comp_4in')
comp_4in_time_tic = tic;
[vclmn,mat1,mat2,indices] = comp_4in(sqp1,[],my_rank);
comp_4in_time = toc(comp_4in_time_tic);
save(time_file, 'comp_4in_time','-append');
%disp('Finaliza comp_4in');

%toc
%disp('Finalizo comp_4in')
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%vname=strcat('V',mdatos);
%save(vname,'vt','vs','vv','sqp1','sz','ldiv') 
%contr_v(vname,vt,vs,vv);
%matname=strcat('MAT',mdatos);
%save(matname,'vclmn','mat1','mat2','indices','md','centros')
%contr_mat(matname,vclmn,mat1,mat2);

%[energy,autoval,cs,pse,ctr,s]=metodo(vt,vv,...
%	vs,mat1,mat2,sz,sqp1,clsmd);
return
					














