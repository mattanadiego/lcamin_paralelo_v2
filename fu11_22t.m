% Funcion que se integra en la rutina mmu_22.(1sa 1sa 1gb 1gc)
function [fu]=fu11_22t(u,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
t=te12_22(u',a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
f0=efe0(t);
num=exp(-u'.^2).*u'.^3;
den=(alfa1+alfa2)^2+4*u.^2*(beta1+beta2);
den=sqrt(den);
fu=num./den' ;
fu=(fu.*f0)';
return


% ================================================
% Procedimiento que genera el t de la funcion f0
function [t]=te12_22(u,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
%--------------------
%Psigma,lambda
y2=beta1*b1+beta2*b2;
y2=y2/(beta1+beta2);
yy=y2-a1;
norma=yy'*yy;
%----------
t=(alfa1+alfa2)^2+4*u.^2*(beta1+beta2);
t=(alfa1+alfa2)^2*(beta1+beta2)./t;
t=t.*norma;
return

