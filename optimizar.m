%
% Based upon bfgswopt.m de C. T. Kelley, July 17, 1997
%
% This code comes with no guarantee or warranty of any kind.
%
% function [x,histout] = bfgswopt(x0,f,tol,maxit,hess0)
%
% steepest descent/bfgs with polynomial line search
% Steve Wright storage of H^-1
%
% if the BFGS update succeeds 
% backtrack on that with a polynomial line search, otherwise we use SD
%
% Input: x0 = initial iterate
%        f = objective function,
%            the calling sequence for f should be
%            [fout,gout]=f(x) where fout=f(x) is a scalar
%              and gout = grad f(x) is a COLUMN vector
%        tol = termination criterion norm(grad) < tol
%              optional, default = 1.d-6
%        maxit = maximum iterations (optional) default = 20
% Output: x = solution
%         histout = iteration history   
%             Each row of histout is
%       [norm(grad), f, num step reductions, iteration count]
%         costdata = [num f, num grad] 
function [x,fc,histout,ctrlend,costdata] = optimizar(f,x0,mum,tol,...
   chgd,TolFun,maxit,indp)
blow=.1; bhigh=.5;
numf=0; numg=0;
costdata=[];
switch nargin
case 7
   indp=1:length(x0); 
case 6
   maxit=20; 
   indp=1:length(x0); 
case 5
   TolFun=1e-4*ones(size(x0));
   maxit=20; 
   indp=1:length(x0); 
case 4
   chgd=1e-4*ones(size(x0));
   TolFun=1e-4*ones(size(x0));
   maxit=20; 
   indp=1:length(x0);
case 3
   tol=1e-6;
   chgd=1e-4*ones(size(x0));
   TolFun=1e-4*ones(size(x0));
   maxit=20; 
   indp=1:length(x0); 
case 2
   mum=[];
   tol=1e-6;
   chgd=1e-4*ones(size(x0));
   TolFun=1e-4*ones(size(x0));
   maxit=20; 
   indp=1:length(x0); 
end
chgd0=chgd;
itc=1; xc=x0;
maxarm=10; nsmax=50; debug=0;
%
n=length(x0);
fc=feval(f,x0,[]);
[gc,ctrlchg]=gradiente(f,fc,x0,chgd,TolFun);
%	Aumento del paso cuando se est� por debajo de 
%	la tolerancia al calcular los gradientes
chgd=(10.^ctrlchg).*chgd;
numf=numf+1; 
numg=numg+1;
ithist=zeros(maxit,2+2*length(x0));
ithist(1,1) = fc;
ithist(1,2:1+length(x0))=x0;
ithist(1,2+length(x0):1+2*length(x0))=gc';
ithist(1,2+2*length(x0))=0; 
%
go=zeros(n,1); 
alpha=zeros(nsmax,1); beta=alpha;
sstore=zeros(n,nsmax); 
ns=0;
%
%	dsdp = - H_c^{-1} grad_+ if ns > 0
%
ctrlend=1;
while itc <= maxit
   fold=fc;
   if rem(itc,5)==0
      chgd=chgd0;
   end
   if rem(itc,2)==0
%      pause;
   end
   dsd=-gc;
   dsdp=-gc;
   if (ns>1)
      dsdp=bfgsw(sstore,alpha,beta,ns,dsd);
   end
   %
   % compute the direction
   %
   if (ns==0) 
      dsd=-gc;
   else
      xi=-dsdp;
      b0=-1/(s*y);
      zeta=(1-1/lambda)*s'+xi;
      a1=b0*b0*(zeta'*y);
      a1=-b0*(1 - 1/lambda)+b0*b0*y'*xi;
      a=-(a1*s+b0*xi')*gc;
      %		We save go=s'*g_old just so we can use it here
      %		and avoid some cancellation error
      alphatmp=a1+2*a/go;
      b=-b0*go;
      dsd=(a*s'+b*xi);
   end
   %
   if (dsd'*gc > -1.d-6*norm(dsd)*norm(gc))
      disp(' loss of descent')
      [itc, dsd'*gc]
      dsd=-gc;
      ns=0;
   end
   lambda=1; 
   %  fixup against insanely long steps see (3.50) in the book
   lambda=min(1,100/(1 + norm(gc)));
   xt=xc+lambda*dsd';
   [xt,lambda]=ctrlpos(lambda,xc,dsd,indp);
   ft=feval(f,xt,[]); 
   numf=numf+1;
   itc=itc+1; 
   ctrlold1=1;
   iarm=0; goalval=.0001*(dsd'*gc);
   q0=fc; 
   qp0=gc'*dsd; 
   lamc=lambda; 
   qc=ft;
   while(ft > fc + lambda*goalval )
      iarm=iarm+1;
      if iarm==1
         lambda=polymod(q0, qp0, lamc, qc, blow, bhigh);
      else
         lambda=polymod(q0, qp0, lamc, qc, blow, bhigh, lamm, qm);
      end
      qm=qc; lamm=lamc; 
      xt=xc+lambda*dsd';
      [xt,lambda]=ctrlpos(lambda,xc,dsd,indp);
      lamc=lambda;
      ft=feval(f,xt,mum); 
      qc=ft; numf=numf+1;
      if(iarm > maxarm) 
         x=xc; histout=ithist(1:itc,:);
         disp(' too many backtracks in BFGS line search');
         return; 
      end
      ctrlold1=0;
   end
      
   s=xt-xc; y=gc; go=s*gc;
   %  lambda=norm(s)/norm(dsd);
   xc=xt;
   fc=ft;
   [gc,ctrlchg]=gradiente(f,fc,xc,chgd,TolFun);
   %	Aumento del paso cuando se est� por debajo de 
   %	la tolerancia al calcular los gradientes
   chgd=(10.^ctrlchg).*chgd;
   y = gc-y; 
   numf=numf+1; 
   numg=numg+1;
   %
   %   restart if y'*s is not positive or we're out of room
   %
   if (s*y <= 0) | (ns==nsmax) 
      disp(' loss of positivity or storage'); 
      [ns, s*y]
      ns=0;
   else
      ns=ns+1; sstore(:,ns)=s';
      if(ns>1)
         alpha(ns-1)=alphatmp;
         beta(ns-1)=b0/(b*lambda);
      end
   end
   ithist(itc,1) = fc;
   ithist(itc,2:1+length(x0))=xc;
   ithist(itc,2+length(x0):1+2*length(x0))=gc';
   ithist(itc,2+2*length(x0))=norm(dsd); 
   save historia.txt ithist -ASCII -DOUBLE -TABS
   %
   % 	Criterios de finalizaci�n
   %
   if ((-(dsd'*gc)*abs(fc) <= tol) & (dsd'*gc)<0)
      ctrlend=1;
      break;
   end
   if (all(abs(dsd)<chgd0'))
      ctrlend=2;
      break;
   end
   if abs((fc-fold)/fold)<TolFun
      ctrlend=3;
      break;
   end
end

x=xc
histout=ithist(1:itc,:); 
costdata=[numf, numg];
return
%
% bfgsw
% C. T. Kelley, Dec 20, 1996
% This code comes with no guarantee or warranty of any kind.
% This code is used in bfgswopt.m 
% There is no reason to ever call this directly.
% form the product of the bfgs approximate inverse Hessian
% with a vector using the Steve Wright method
%
function dnewt=bfgsw(sstore,alpha,beta,ns,dsd)
dnewt=dsd; 
if (ns<=1) 
   return; 
end;
dnewt=dsd; 
n=length(dsd);
sigma=sstore(:,1:ns-1)'*dsd; 
gamma1=alpha(1:ns-1).*sigma;
gamma2=beta(1:ns-1).*sigma;
gamma3=gamma1+beta(1:ns-1).*(sstore(:,2:ns)'*dsd);
delta=gamma2(1:ns-2)+gamma3(2:ns-1);
dnewt=dnewt+gamma3(1)*sstore(:,1)+gamma2(ns-1)*sstore(:,ns);
if(ns <=2) 
   return; 
end
dnewt=dnewt+sstore(1:n,2:ns-1)*delta(1:ns-2);
return





