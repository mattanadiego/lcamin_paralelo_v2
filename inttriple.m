function [fxi]=inttriple(F,limx,puntosx,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20
global intq24 intq32 intq40 intq60 intq80 intq100 intq120
global terxyz24
if isempty(puntosx)
   puntosx=24
end

switch puntosx
case 2
   intqx=intq2;
case 3
   intqx=intq3;
case 4
   intqx=intq4;
case 6
   intqx=intq6;
case 8
   intqx=intq8;
case 12
   intqx=intq12;
case 16
   intqx=intq16;
case 20
   intqx=intq20;
case 24
   intqx=intq24;
case 32
   intqx=intq32;
case 40
   intqx=intq40;
case 60
   intqx=intq60;
case 80
   intqx=intq80;
case 100
   intqx=intq100;
case 120
   intqx=intq120;
otherwise
   intqx=intq12;
end

ex = intqx(:,1);
wx = intqx(:,2);
xs=limx(2);
xi=limx(1);
diffx = xs' - xi';
xcx = 0.5*( (xi'+xs')+(diffx .* ex'));
if (~isempty(terxyz24)&puntosx==24)
   parxyz=terxyz24(:,1:3);
   pesxyz=terxyz24(:,4);
else
   % Contruyo las ternas (x,y,z)
   %parxy=repmat(xcx',lxcx,1);
   %parxy=[parxy,reshape(repmat(xcx,1,lxcx),lxcx^2,1)];
   %pesxy=reshape(wx*wx',lxcx^2,1);
   %parxyz=repmat(parxy,lxcx,1);
   %parxyz=[parxyz,reshape(repmat(xcx,1,lxcx),lxcx^3,1)];
   %pesxyz=reshape(pesxy*wx',lxcx^3,1);
   parxy=[];
   pesxy=[];
   lxcx=length(xcx);
   for j=1:lxcx
      parxy=[parxy;[xcx' ones(lxcx,1)*xcx(j)]];
      pesxy=[pesxy;wx*wx(j)];
   end
   parxyz=[];
   pesxyz=[];
   lxcxy=length(parxy);
   for j=1:lxcx
      parxyz=[parxyz;[parxy ones(lxcxy,1)*xcx(j)]];
      pesxyz=[pesxyz;pesxy*wx(j)];
   end
end

[fx]=feval(F,parxyz,varargin{:});
fxi=(1/8)*(diffx.^3)*(fx*pesxyz);
return

   
