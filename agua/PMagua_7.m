function [pinicial,mum,chgd,indp,corrida]=pmagua
% jorge ast. corrida='2'
global objetivo
objetivo='agua';
corrida=4
pinicial=[2]; %0.065226; 3.41823; %3

mum=[1:7];
%1.719 0.3814   1e-3 1e-3
%corrida='3'
%pinicial=[0.2 0.7 3.5 0.5 0.172 3.20710  2.68844 1.04578];

%En la matriz mum se indican, por filas, los
%orbitales que afecta cada par�metro. Si 
%mum=[3 6; 5 0] resulta que el parametro 1 afecta
%a los orbitales 3 y 6 y el parametro 2 solo al
%orbital 5. 
% Parametros: 
%	  13  14  10  11  1   2   3
%mum=[5 7 0 0;6 8 0 0;3 0 0 0;4 0 0 0;5 6 7 8;2 4 0 0;1 0 0 0;2 0 0 0];
%mum =[
%     5     7     0     0;
%     6     8     0     0;
%     3     0     0     0;
%     4     0     0     0;
%     5     6     7     8;
%     2     4     0     0;
%     1     0     0     0;
%     2     0     0     0;]
%Energ�a final: -75.37092429553897
%pfinal=[2.74936044277093   0.50319455127418]

chgd=[1e-3];

%
% 		Indicar en chgd los incrementos para usar 
%		en el c�lculo de las derivadas parciales
% 		respecto de cada par�metro en pinicial
%

%
% 		Indicar en indp los par�metros de pinicial 
%		que deben mantenerse positivos
%
indp=1:length(pinicial);
%
if ~(ischar(corrida))
   corrida='000';
end

return
 
