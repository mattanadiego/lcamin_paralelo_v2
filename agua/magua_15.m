function [md,centros,ldiv]=magua_15(param0)
global mdatos
mdatos='magua_15';
% Calculo para Oxigeno solo 21/5/2008
sel=[1:15];
param=[
   2;      %1
   0.064613; %2
   3.298912; %3
   1.3;     %4         
   54.73561;   %5
   2.5 ;  %6
   54.73561; %7   
   0.4;%8
   0.5;%9   
   7.8;%10
   52.25;%11
   0.7];%12
if ~isempty(param0)
   param(2)=param0(1);
 %  param(3)=param0(2);
%   param(10)=param0(3);
%   param(9)=param0(4);
end
%sr1=param(2)*param(3)^5;
%sr2=param(2)*param(3)^4;
sr3=param(2)*param(3)^3;
sr4=param(2)*param(3)^2;
sr5=param(2)*param(3);

epjor=0.1;
%R1=epjor*0.5*((3/sr1)^0.5);
%R2=epjor*0.5*((3/sr2)^0.5);
R3=epjor*0.5*((3/sr3)^0.5);
R4=epjor*0.5*((3/sr4)^0.5);
R5=epjor*0.5*((3/sr5)^0.5);

%cz1=R1*cos(pi*param(5)/180);
%cy1=R1*sin(pi*param(5)/180);
%cz2=R2*cos(pi*param(5)/180);
%cy2=R2*sin(pi*param(5)/180);
cz3=R3*cos(pi*param(5)/180);
cy3=R3*sin(pi*param(5)/180);
cz4=R4*cos(pi*param(5)/180);
cy4=R4*sin(pi*param(5)/180);
cz5=R5*cos(pi*param(5)/180);
cy5=R5*sin(pi*param(5)/180);

%pz1=R1*cos(pi*param(5)/180);
%px1=R1*sin(pi*param(5)/180);
%pz2=R2*cos(pi*param(5)/180);
%px2=R2*sin(pi*param(5)/180);
pz3=R3*cos(pi*param(5)/180);
px3=R3*sin(pi*param(5)/180);
pz4=R4*cos(pi*param(5)/180);
px4=R4*sin(pi*param(5)/180);
pz5=R5*cos(pi*param(5)/180);
px5=R5*sin(pi*param(5)/180);

cyh=param(1)*sin(pi*param(11)/180);
czh=param(1)*cos(pi*param(11)/180);

%    0       cyh	     czh     1        1;
%    0      -cyh	     czh     1        1;


%faj=0.5

%    0       cyh*faj  faj*czh  0.9       2;
%    0      -cyh*faj  faj*czh  0.9       2;
%    0       cy1	     cz1     sr1      2; %2
%    0      -cy1	     cz1     sr1      2; %3
%  px1         0	    -cz1     sr1      2; %4
% -px1         0	    -cz1     sr1      2; %5
%    0       cy2	     cz2     sr2      2; %6
%    0      -cy2	     cz2     sr2      2; %7
%  px2         0	    -cz2     sr2      2; %8
% -px2         0	    -cz2     sr2      2; %9


md=[0         0          0   param(10)  1; %1
   0       cyh	     czh     1        1;
   0      -cyh	     czh     1        1;
    0       cy3	     cz3     sr3      2; %10
    0      -cy3	     cz3     sr3      2; %11
  px3         0	    -cz3     sr3      2; %12
 -px3         0	    -cz3     sr3      2; %13
    0       cy4	     cz4     sr4      2; %14
    0      -cy4	     cz4     sr4      2; %15
  px4         0	    -cz4     sr4      2; %16
 -px4         0	    -cz4     sr4      2; %17
    0       cy5	     cz5     sr5      2; %18
    0      -cy5	     cz5     sr5      2; %19
  px5         0	    -cz5     sr5      2; %20
 -px5         0	    -cz5     sr5      2]; %21


%%%%%%%%%%%%%%%%%
md=md(sel,:);
ldiv=sum(md(:,5)==1);

%Definici�n de los centros
centros=[];
centros=md([1:3],1:3);
%centros=md([1:1],1:3);

if (isempty(centros)&& ldiv)
   centros=md(1:ldiv,1:3);
end
centros=centros';

% Valores de los z. La suma debe ser par. La longitud de zhh debe
% coincidir con la las columnas de centros. 
if ~isempty(centros)
 % zhh=[6 1 1];
 zhh=[8 1 1];
 %zhh=[8];
 %zhh=[16];
 %zhh=[16 1 1];
   centros=[centros;zhh];
end
md=md';
return
