function [md,centros,ldiv]=magua(param0)
global mdatos
global alfa pp2 pp3 %20 de junio 2011

mdatos='MAGUAmuchas';
% Calculo para Oxigeno solo 21/5/2008
sel=[1:50];
param=[
   1.81;      %1
   pp2; %0.09315017573478;	  % 0.064613; %2
   pp3 %5.84688378576822; %5.7;  %3.919% 3.298912; %3
   1.3;     %4         
   54.73561;   %5
   2.5 ;  %6
   54.73561; %7   
   0.4;%8
   0.5;%9   
   alfa     %15.54061141028626; %7.8;%10
   52.25;%11
   0.7];%12

if ~isempty(param0)
   param(2)=param0(2);
   param(3)=param0(3);
   param(10)=param0(1);
%   param(9)=param0(4);
end
sr1=param(2)*param(3)^5;
sr2=param(2)*param(3)^4;
sr3=param(2)*param(3)^3;
sr4=param(2)*param(3)^2;
sr5=param(2)*param(3);

epjor=0.1;
R1=epjor*0.5*((3/sr1)^0.5);
R2=epjor*0.5*((3/sr2)^0.5);
R3=epjor*0.5*((3/sr3)^0.5);
R4=epjor*0.5*((3/sr4)^0.5);
R5=epjor*0.5*((3/sr5)^0.5);

cz1=R1*cos(pi*param(5)/180);
cy1=R1*sin(pi*param(5)/180);
cz2=R2*cos(pi*param(5)/180);
cy2=R2*sin(pi*param(5)/180);
cz3=R3*cos(pi*param(5)/180);
cy3=R3*sin(pi*param(5)/180);
cz4=R4*cos(pi*param(5)/180);
cy4=R4*sin(pi*param(5)/180);
cz5=R5*cos(pi*param(5)/180);
cy5=R5*sin(pi*param(5)/180);

pz1=R1*cos(pi*param(5)/180);
px1=R1*sin(pi*param(5)/180);
pz2=R2*cos(pi*param(5)/180);
px2=R2*sin(pi*param(5)/180);
pz3=R3*cos(pi*param(5)/180);
px3=R3*sin(pi*param(5)/180);
pz4=R4*cos(pi*param(5)/180);
px4=R4*sin(pi*param(5)/180);
pz5=R5*cos(pi*param(5)/180);
px5=R5*sin(pi*param(5)/180);

%cyh=param(1)*sin(pi*param(11)/180);
%czh=param(1)*cos(pi*param(11)/180);

%    0       cyh	     czh     1        1;
%    0      -cyh	     czh     1        1;


%faj=0.5

%    0       cyh*faj  faj*czh  0.9       2;
%    0      -cyh*faj  faj*czh  0.9       2;
a=1;
md=[
         0    0      0    8.000    1.0000 %1
         0    a      0    8.0000   1.0000 %2
         0    2*a    0    8.0000   1.0000 %3
         0    3*a    0    1.0000   2.0000 %4
         0    4*a    0    1.0000   2.0000 %5
         0    0      a    1.0000   2.0000 %6
         0    a      a    1.0000   2.0000 %7
         0    2*a    a    1.0000   2.0000 %8
         0    3*a    a    1.0000   2.0000 %9
         0    4*a    a    1.0000   2.0000 %10
         0    0     2*a   1.0000   2.0000 %11
         0    a      2*a    1.0000   2.0000 %12
         0    2*a    2*a    1.0000   2.0000 %13
         0    3*a    2*a    1.0000   2.0000 %14
         0    4*a    2*a    1.0000   2.0000 %15
         0    0      3*a    1.0000   2.0000 %16
         0    a      3*a    1.0000   2.0000 %17
         0    2*a    3*a    1.0000   2.0000 %18
         0    3*a    3*a    1.0000   2.0000 %19
         0    4*a    3*a    1.0000   2.0000 %20
         0    0      4*a    1.0000   2.0000 %21
         0    a      4*a    1.0000   2.0000 %22
         0    2*a    4*a    1.0000   2.0000 %23
         0    3*a    4*a    1.0000   2.0000 %24
         0    4*a    4*a    1.0000   2.0000 %25
         
         a    0      0    1.000    2.0000 %26
         a    a      0    1.0000   2.0000 %27
         a    2*a    0    1.0000   2.0000 %28
         a    3*a    0    1.0000   2.0000 %29
         a    4*a    0    1.0000   2.0000 %30
         a    0      a    1.0000   2.0000 %31
         a    a      a    1.0000   2.0000 %32
         a    2*a    a    1.0000   2.0000 %33
         a    3*a    a    1.0000   2.0000 %34
         a    4*a    a    1.0000   2.0000 %35
         a    0     2*a   1.0000   2.0000 %36
         a    a      2*a    1.0000   2.0000 %37
         a    2*a    2*a    1.0000   2.0000 %38
         a    3*a    2*a    1.0000   2.0000 %39
         a    4*a    2*a    1.0000   2.0000 %40
         a    0      3*a    1.0000   2.0000 %41
         a    a      3*a    1.0000   2.0000 %42
         a    2*a    3*a    1.0000   2.0000 %43
         a    3*a    3*a    1.0000   2.0000 %44
         a    4*a    3*a    1.0000   2.0000 %45
         a    0      4*a    1.0000   2.0000 %46
         a    a      4*a    1.0000   2.0000 %47
         a    2*a    4*a    1.0000   2.0000 %48
         a    3*a    4*a    1.0000   2.0000 %49
         a    4*a    4*a    1.0000   2.0000 %50
        ];

%%%%%%%%%%%%%%%%%
md=md(sel,:);
ldiv=sum(md(:,5)==1);

%Definición de los centros
centros=[];
centros=md([1:3],1:3);
%centros=md([1:1],1:3);

if (isempty(centros)& ldiv)
   centros=md(1:ldiv,1:3);
end
centros=centros';

% Valores de los z. La suma debe ser par. La longitud de zhh debe
% coincidir con la las columnas de centros. 
if ~isempty(centros)
 % zhh=[6 1 1];
 zhh=[8 8 8];
% zhh=[8];
 %zhh=[16]; %azufre
 %zhh=[16 1 1];
   centros=[centros;zhh];
end
md=md';
return