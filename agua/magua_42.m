function [md,centros,ldiv]=magua_42(param0)
global mdatos
% global alfa pp2 pp3 

mdatos='magua_42';
% Calculo para O2
sel=[1:42]; %Number of the functions.
param=[
   3;      %1  Distance between the centers (O-O)
   0.0647861; %2   a
   3.318884;  %3   b, with alfa(i) = a * b^(i) and i is the correp tetrahedron 
   1.3;       %4         
   54.73561;  %5  (Angle lone pairs)/2 
   2.5 ;      %6
   54.73561;  %7   Angle of the tetrahedron 
   0.4;       %8   localization, on the bond length, of the floating gaussian 1s near H�s
   0.5;       %9   
   8;       %10  alpha 1s STO (almost atomic number of the Oxigen)
   52.25;     %11 
   0.7];      %12
   
   
if ~isempty(param0) %Only for the optimizations
   param(1)=param0(1);
%   param(3)=param0(3);
%   param(10)=param0(1);
%  param(9)=param0(4);
end

% sri represents to alpha_i, size of the Gaussians
sr1=param(2)*param(3)^5;   % Biggest alpha_i, closest to the nucleous
sr2=param(2)*param(3)^4;
sr3=param(2)*param(3)^3;
sr4=param(2)*param(3)^2;
sr5=param(2)*param(3);

epjor=0.1;

% Distances Gaussians - nucleous
R1=epjor*0.5*((3/sr1)^0.5);
R2=epjor*0.5*((3/sr2)^0.5);
R3=epjor*0.5*((3/sr3)^0.5);
R4=epjor*0.5*((3/sr4)^0.5);
R5=epjor*0.5*((3/sr5)^0.5);

% Coordinates on (y,z) plane (O contributions to the O2 bond)
cz1=R1*cos(pi*param(5)/180);
cy1=R1*sin(pi*param(5)/180);
cz2=R2*cos(pi*param(5)/180);
cy2=R2*sin(pi*param(5)/180);
cz3=R3*cos(pi*param(5)/180);
cy3=R3*sin(pi*param(5)/180);
cz4=R4*cos(pi*param(5)/180);
cy4=R4*sin(pi*param(5)/180);
cz5=R5*cos(pi*param(5)/180);
cy5=R5*sin(pi*param(5)/180);

% Coordinates on (x,z) plane (O contributions to the O2 lone pairs)

pz1=R1*cos(pi*param(5)/180);
px1=R1*sin(pi*param(5)/180);
pz2=R2*cos(pi*param(5)/180);
px2=R2*sin(pi*param(5)/180);
pz3=R3*cos(pi*param(5)/180);
px3=R3*sin(pi*param(5)/180);
pz4=R4*cos(pi*param(5)/180);
px4=R4*sin(pi*param(5)/180);
pz5=R5*cos(pi*param(5)/180);
px5=R5*sin(pi*param(5)/180);

%cyh=param(1)*sin(pi*param(11)/180);
%czh=param(1)*cos(pi*param(11)/180);

%    0       cyh	     czh     1        1;
%    0      -cyh	     czh     1        1;

tr=param(1);
md=[
         0    0     0    param(10)    1      %1
         0    0     tr    param(10)    1      %2
         
         0    cy1	  cz1  sr1          2;     %3
         0   -cy1	  cz1  sr1          2;     %4
         
         px1  0	  -pz1  sr1          2;     %5
         -px1  0	  -pz1  sr1          2;     %6
         
         0    cy2	  cz2  sr2          2;     %7
         0   -cy2	  cz2  sr2          2;     %8
         
         px2  0	  -pz2  sr2          2;     %9
         -px2  0	  -pz2  sr2          2;     %10
         
         0    cy3	  cz3  sr3          2;     %11
         0   -cy3	  cz3  sr3          2;     %12
         px3  0	  -pz3  sr3          2;     %13
        -px3  0	  -pz3  sr3          2;     %14
         0    cy4	  cz4  sr4          2;     %15
         0   -cy4	  cz4  sr4          2;     %16
         px4  0	  -pz4  sr4          2;     %17
        -px4  0	  -pz4  sr4          2;     %18
         0    cy5	  cz5  sr5          2;     %19
         0   -cy5	  cz5  sr5          2;     %20
         px5  0	  -pz5  sr5          2;     %21
         -px5  0	  -pz5  sr5          2;     %22
         
         0    cy1	  tr-cz1  sr1        2;     %23
         0   -cy1	  tr-cz1  sr1        2;     %24
         
         px1  0	  tr+pz1  sr1          2;     %25
        -px1  0	  tr+pz1  sr1          2;     %26
        
         0    cy2	  tr-cz2  sr2        2;     %27
         0   -cy2	  tr-cz2  sr2        2;     %28
         
         px2  0	  tr+pz2  sr2          2;     %29
        -px2  0	  tr+pz2  sr2          2;     %30
        
        0    cy3	  tr-cz3  sr3        2;     %31
        0   -cy3	  tr-cz3  sr3        2;     %32
        
         px3  0	  tr+pz3  sr3          2;     %33
         -px3  0	  tr+pz3  sr3          2;     %34
         
         0    cy4	  tr-cz4  sr4        2;     %35
         0   -cy4	  tr-cz4  sr4        2;     %36
         
         px4  0	  tr+pz4  sr4          2;     %37
         -px4  0	  tr+pz4  sr4          2;     %38
         
         0    cy5	  tr-cz5  sr5        2;     %39
         0   -cy5	  tr-cz5  sr5        2;     %40
         
         px5  0	  tr+pz5  sr5          2;     %41
        -px5  0	  tr+pz5  sr5          2];     %42
        

%%%%%%%%%%%%%%%%%
md=md(sel,:);
ldiv=sum(md(:,5)==1);

%Definici�n de los centros

centros=md(1:2,1:3);
%centros=md([1:1],1:3); atomos

if (isempty(centros)&& ldiv)
   centros=md(1:ldiv,1:3);
end
centros=centros';

% Valores de los z. La suma debe ser par. La longitud de zhh debe
% coincidir con la las columnas de centros. 
if ~isempty(centros)
 % zhh=[6 1 1];
 zhh=[8 8];
% zhh=[8];
 %zhh=[16]; %azufre
 %zhh=[16 1 1];
   centros=[centros;zhh];
end
md=md';
return
