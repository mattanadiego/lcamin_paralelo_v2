% Esta funci�n describe el integrando utilizado en m11_m11c
function [fu]=fu11_11c(ternas,a1,alfa1,a2,alfa2,a3,alfa3,a4,alfa4)
u1=ternas(:,1);
u2=ternas(:,2);
u3=ternas(:,3);
a12=a1-a2;
a34=a3-a4;
a12=norm(a12);
a34=norm(a34);
p1=p(u1,u2,a1,a2,a3,a4);
j0=ones(length(u3),1);
ip1=find(p1);
if ~isempty(ip1)
   j0(ip1)=sin(p1(ip1).*u3(ip1))./(p1(ip1).*u3(ip1));
end
z1=(alfa2^2+(alfa1^2-alfa2^2)*u1)+(u1.*(1-u1)).*(u3.^2);
z1=sqrt(z1)*a12;
z2=alfa4^2+(alfa3^2-alfa4^2)*u2;
z2=(z2*ones(1,size(u3,2)))+((u2.*(1-u2))*ones(1,size(u3,2))).*...
   (u3.^2);
z2=sqrt(z2)*a34;
fu=(u1.*(1-u1)).*((u2.*(1-u2))*ones(1,size(u1,2))).*...
   j0.*k1(z1).*k1(z2);
fu=fu';
return;


% -----------------------------------------------
function [k11]=k1(q)
	k11=q.^(-5)*(pi/2)^(0.5).*exp(-q);
	k11=k11.*(3+3*q+q.^2);
return

% -----------------------------------------------
function [p1]=p(u,v,a1,a2,a3,a4)	
	px=(u*a2(1)+(1-u)*a1(1))-(v*a4(1)+...
      (1-v)*a3(1))*ones(1,size(u,2));
  py=(u*a2(2)+(1-u)*a1(2))-(v*a4(2)+...
     (1-v)*a3(2))*ones(1,size(u,2));
  pz=(u*a2(3)+(1-u)*a1(3))-(v*a4(3)+...
     (1-v)*a3(3))*ones(1,size(u,2));
  p1=sqrt((px.*px+py.*py+pz.*pz));
return

