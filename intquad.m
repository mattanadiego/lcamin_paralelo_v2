
%** INTEGRAL.SRC - integration using Gauss-Legendre quadrature.
%** =========================================================================
%**   Procedure     Format                       Purpose               Line
%** =========================================================================
%**   INTQUAD1      y = INTQUAD1(&f,xl);         1-D integraton          18

%** INTQUAD1
%**
%** Purpose:    Integrates a specified function using Gauss-Legendre
%**             quadrature. A suite of upper and lower bounds may be
%**             calculated in one procedure call.
%**
%** Format:     y = INTQUAD1(&f,xl);
%**
%** Input:      &f   pointer to the procedure containing the function
%**                  to be integrated.
%**
%**             xl   2xN matrix, the limits of x.
%**
%**                  The first row is the upper limit and the second
%**                  row is the lower limit.  N integrations are
%**                  computed.
%**
%**        _intord   scalar, the order of the integration
%**                  2, 3, 4, 6, 8, 12, 16, 20, 24, 32, 40.
%**
%** Output:     y    Nx1 vector of the estimated integral(s) of f(x)
%**                  evaluated between between the limits given by xl.
%**

function [fx]=intquad(F,xi,xs,ptos,varargin);
global intq2 intq3 intq6 intq8 intq12 intq16 intq20
global intq32 intq40 intq60 intq80 intq100 intq120
switch ptos
case 2
   intq=intq2;
case 3
   intq=intq3;
case 4
   intq=intq4;
case 6
   intq=intq6;
case 8
   intq=intq8;
case 12
   intq=intq12;
case 16
   intq=intq16;
case 20
   intq=intq20;
case 24
   intq=intq24;
case 32
   intq=intq32;
case 40
   intq=intq40;
case 60
   intq=intq60;
case 80
   intq=intq80;
case 100
   intq=intq100;
case 120
   intq=intq120;
otherwise
   intq=intq12;
end

e = intq(:,1);
w = intq(:,2);
diff = xs' - xi';
xc = 0.5*( (xi'+xs')+(diff .* e'));
fx = feval(F,xc,varargin{:});
fx = zeros(size(xc)) + feval(F,xc,varargin{:});
fx = ((diff/2).* (fx*w));
return

