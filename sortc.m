% Esta funci�n ordena la matriz x por la n-esima columna.
% Retorna la matriz reordenada en xs y las permutaciones 
%realizadas en ind

function [xs,ind]=sortc(x,n)
	[y,ind]=sort(x(:,n));
   xs=x(ind,:);
return
