%function [vsf,mat1,mat2,vindm]=comp_4in(md,ldiv,sqp1,vclmn,mat1,mat2,vindm)
%function [vsf,mat1,mat2,vindm]=comp_4in(mdIN,ldiv,sqp1,vclmn)
function [vsf,mat1,mat2,vindm]=comp_4in(sqp1,vclmn,my_rank)
global orden mat1 mat2 vindm rank

if nargin==3
   vindm=[];
end

if isempty(orden)
   orden = 0;
end

md = loadmat('md');
ldiv = loadmat('ldiv');
sqp1_name = strcat('sqp1_',num2str(my_rank));
sqp1 = loadmat(sqp1_name);
%sqp1 = load(sqp1_name);

clsmd=size(md,2); %cantidad de columnas
clsmd2=clsmd^2;

%sqp1 = reshapeSqp1(sqp1);
rsqp1=size(sqp1,1);

globales
%if isempty(vindm)
    
   mat1=zeros(clsmd2^2,1);
   mat2=zeros(clsmd2^2,1);
   vsf=[];

%   tic
   for i=1:size(sqp1,1); %recorro todas las filas
      a1=md(1:3,sqp1(i,1));
      alfa1=md(4,sqp1(i,1));
      a2=md(1:3,sqp1(i,2));
      alfa2=md(4,sqp1(i,2));
      b1=md(1:3,sqp1(i,3));
      beta1=md(4,sqp1(i,3));
      b2=md(1:3,sqp1(i,4));
      beta2=md(4,sqp1(i,4));
      cpar=ctrlpar(sqp1(i,1:2),...
            sqp1(i,3:4),ldiv,a1,alfa1,...
            a2,alfa2,b1,beta1,b2,beta2);
      %vindm=[vindm;[sqp1(i,:)]];
      
      fxr=calc_int(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,cpar,orden);
      
      vsf=[vsf;fxr];
      [mat1,mat2]=llenamat(fxr,mat1,mat2,...
            sqp1(i,1),sqp1(i,2),...
            sqp1(i,3),sqp1(i,4),clsmd,sqp1);

   end

%   toc
%else
%   vsf=vclmn;
%   tic
%   for kk=1:size(vindm,1)
%      mu=vindm(kk,1);
%      nu=vindm(kk,2);
%      ro=vindm(kk,3);
%      sg=vindm(kk,4);
%      a1=md(1:3,mu);
%      alfa1=md(4,mu);
%      a2=md(1:3,nu);
%      alfa2=md(4,nu);
%      b1=md(1:3,ro);
%      beta1=md(4,ro);
%      b2=md(1:3,sg);
%      beta2=md(4,sg);
%      cpar=ctrlpar([mu nu],[ro,sg],ldiv,a1,alfa1,...
%            a2,alfa2,b1,beta1,b2,beta2);
%      fxr=calc_int(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,cpar,orden);
%      vsf(vindm(kk,5))=fxr;
%      [mat1,mat2]=llenamat(fxr,mat1,mat2,...
%         mu,nu,ro,sg,clsmd,sqp1);
%   end
%   toc
%end
%disp('Finalizo comp_4in')

name_mat1 = strcat('mat1_',num2str(my_rank));
name_mat2 = strcat('mat2_',num2str(my_rank));
%save(name,'mat1') ;
savemat(name_mat1,3,mat1);

savemat(name_mat2,3,mat2);
name_vsf = strcat('vsf_',num2str(my_rank));
savemat(name_vsf,3,vsf);

vindm = sqp1;

return

% ================================================

function [fxr]=calc_int(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,cpar,orden)
switch cpar
case '1b_1b  '
   fxr=m1b_m1b(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_11t1'
   fxr=m11_m11t(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_11t2'
   fxr=m11_m11t(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2);
case '11_11  '
   switch orden
   case 0
      fxr=m11_m11c(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   otherwise
      fxr=m11_m11cma(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,orden);
   end
case '11_12c1'
   fxr=m11_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_12c2'
   fxr=m11_m12(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2); 
case '11_22  '
   fxr=m11_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '12_12  '
   fxr=m12_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '12_22  '
   fxr=m12_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '22_22  '
   fxr=m22_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case 'clmn   '
   fxr=fourind(a1,alfa1,a2,alfa2,...
      b1,beta1,b2,beta2,par1,par2,sqp1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mat1,mat2]=llenamat(fxr,mat1,mat2,...
   mu0,nu0,ro0,sg0,clsmd,sqp1)
%Calculo de mat1
%mu0=sqp1(par1,1);
%nu0=sqp1(par1,2);
%ro0=sqp1(par2,1);
%sg0=sqp1(par2,2);

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

% Cambiamos 
aux1=mu0;
aux2=nu0;
mu0=ro0;		%sqp1(par2,1);
nu0=sg0;		%sqp1(par2,2);
ro0=aux1;		%sqp1(par1,1);
sg0=aux2;		%sqp1(par1,2);

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;   
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      
return

function [mat1,mat2]=llenamatold(fxr,mat1,mat2,par1,par2,clsmd,sqp1)
%Calculo de mat1
mu0=sqp1(par1,1);
nu0=sqp1(par1,2);
ro0=sqp1(par2,1);
sg0=sqp1(par2,2);

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

% Cambiamos 
mu0=sqp1(par2,1);
nu0=sqp1(par2,2);
ro0=sqp1(par1,1);
sg0=sqp1(par1,2);

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;   
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      
return


function [vsf,mat,vind]=comp_4inold(md,ldiv,sqp1)
global vclmn par orden mdatos
global mat
if isempty(orden)
   orden = 0
end
clc
rsqp1=size(sqp1,1);
vsf=[];
vind=[];
clsmd=size(md,2);
clsmd2=clsmd^2;
mat1=zeros(clsmd2^2,1);
mat2=zeros(clsmd2^2,1);
for par1=1:rsqp1
   a1=md(1:3,sqp1(par1,1));
   alfa1=md(4,sqp1(par1,1));
   a2=md(1:3,sqp1(par1,2));
   alfa2=md(4,sqp1(par1,2));
   for par2=par1:rsqp1
      b1=md(1:3,sqp1(par2,1));
      beta1=md(4,sqp1(par2,1));
      b2=md(1:3,sqp1(par2,2));
      beta2=md(4,sqp1(par2,2));
      cpar=ctrlpar(sqp1(par1,:),sqp1(par2,:),ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      vind=[vind;[sqp1(par1,:) sqp1(par2,:)]];
		%tic
      switch cpar
      case 'clmn   '
         fxr=fourind(a1,alfa1,a2,alfa2,...
            b1,beta1,b2,beta2,par1,par2,sqp1);
      case '41     '
         fxr=(5/8)*alfa1;
      case '1b_1b  '
         fxr=m1b_m1b(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '11_11t1'
         fxr=m11_m11t(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '11_11t2'
         fxr=m11_m11t(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2);
      case '11_11  '
         switch orden
         case 0
            fxr=m11_m11c(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
         otherwise
            fxr=m11_m11cma(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,orden);
         end
      case '11_12c1'
         fxr=m11_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '11_12c2'
         fxr=m11_m12(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2); 
      case '11_22  '
         fxr=m11_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '12_12  '
         fxr=m12_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '12_22  '
         fxr=m12_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      case '22_22  '
         fxr=m22_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
      end
      vsf=[vsf;fxr];
      %vind=[vind;[sqp1(par1,:) sqp1(par2,:)]];
      [mat1,mat2]=llenamat(fxr,mat1,mat2,par1,par2,clsmd,sqp1);
      %disp(sqp1(par1,:) sqp1(par2,:)])   
   end
   %disp(sqp1(par1,:))
end
mat=mat1-0.5*mat2;
%%%%%%%%%%%%%
return
