%
% gradiente
%
% J. C. Cesco, August 31, 2001
% This code comes with no guarantee or warranty of any kind.
% This code is used in bfgswopt.m 
%
function [gc,ctrlchg]=gradiente(f,fc,xc,chgd,TolFun)
gc=zeros(size(xc'));  % set to correct size
chg = chgd.*abs(xc)+1e-7*ones(size(xc));
ctrlchg=zeros(size(chg));
for gcnt=1:length(xc)
   xc(gcnt)=xc(gcnt)+sign(xc(gcnt))*chg(gcnt);
   fnew = feval(f,xc,gcnt);
   %%% Agregado mio para tener en cuenta la precisión
   %%%% en el cálculo de f.
   gc(gcnt)=(fnew-fc)/(chg(gcnt));
   if abs((fnew-fc)/abs(fc))<=TolFun
      ctrlchg(gcnt)=1;
   end
   %%%% Fin del agregado
   xc(gcnt)=xc(gcnt)-sign(xc(gcnt))*chg(gcnt);
end
return

