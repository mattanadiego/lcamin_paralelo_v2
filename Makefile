# -l library
#           Search the library named library when linking.  (The second
#           alternative with the library as a separate argument is only for
#           POSIX compliance and is not recommended.)
#
#           The linker searches a standard list of directories for the library,
#           which is actually a file named liblibrary.a.  The linker then uses
#           this file as if it had been specified precisely by name.

#           The directories searched include several standard system
#           directories plus any that you specify with -L.

# -c        Compile or assemble the source files, but do not link. The linking stage 
#           simply is not done. The ultimate output is in the form of an object file 
#           for each source file. 

# -I dir    Add the directory dir to the list of directories to be searched for 
#           header files. Directories named by -I are searched before the standard 
#           system include directories. 

# In order to calculate differences considering the first n-characters from each line (in the example  9 characters):
# diff <(cut -c -9 mat_global) <(cut -c -9 ../lcaomin_jor_sec/ejemplos/mat1)

OCTAVE_VERSION :=4.4.1
OCTAVE_INCLUDE_DIR :=/usr/include/octave-$(OCTAVE_VERSION)/
MPI_INCLUDE_DIR :=/SOFT/mpich/include
LCAOMIN_SRC_DIR :=/home/ltardivo/lcaomin_paralelo
HDF5_DIR :=/usr/include/hdf5/serial
     
all: lcaomain
      
clean:
	rm driver.o lcaomain

clean_mat:
	rm mat1_* mat2_* vsf_* sqp1_*
   
clean_out: 
	-rm jorper RMATmagua* RVmagua* MATmagua* matrices Matrizp niter Vmagua*

mpi_broadcast.o: mpi_broadcast.c
	gcc -c mpi_broadcast.c

#linkea con el interprete de octave 
lcaomain: driver.o mpi_broadcast.c 
	mpicxx -I $(MPI_INCLUDE_DIR) -o lcaomain driver.o  -l octinterp -l octave

driver.o: driver.c
	mpicxx -c driver.c -I $(MPI_INCLUDE_DIR) -I $(OCTAVE_INCLUDE_DIR) -I $(OCTAVE_INCLUDE_DIR)octave/ -I $(LCAOMIN_SRC_DIR) -I $(HDF5_DIR)

# How to run:
# mpirun -np _param1 ./lcaomain _param2 _param3
# _param1: number of processors to use
# _param2: molecule to test
# _param3: number of executions to run
run:
	make clean_mat; make clean_out; mpirun -np 32 ./lcaomain 'magua_15' 1 


