

function [cotita,fi]=Polcord(p);
    mcc=p;
    rv=[];
    rv2=[];
    cmcc=1;

    while cmcc <= size(mcc,2);
        rv=[rv;sqrt(mcc(:,cmcc)'* mcc(:,cmcc))];
        rv2=[rv2;sqrt(mcc(1:2,cmcc)'* mcc(1:2,cmcc))];
        cmcc=cmcc+1;
    end

    cotita=p(3,:)./rv';
    e0=rv2 ~= 0;
    if sum(e0) < length(rv2);
        rv2=rv2+(ones(length(rv2),1)-e0);
    end

    fi=p(1,:) ./ rv2';

    cas  = p(2,:) < 0;
    cas1 = 2*pi * cas;
    fi=acos(fi);
    cas1 = fi + cas1;
    cas1 = cas1 - 2 * fi .* cas;
    fi = cas1;

    % @ Control de p2 = 0 @
    cas  = p(2,:) == 0;
    cas1 = p(1,:) <= 0 ;
    cas = cas .* cas1;
    if sum(cas);
        cas  = find(cas);
        fi(cas) = pi * ones(1,size(cas,1));
    end
    fi = [fi;e0'];
return
