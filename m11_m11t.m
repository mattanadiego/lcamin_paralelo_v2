% Esta rutina eval�a las integrales cuatric�ntricas (1sa 1sa|1sc 1sd)
% seg�n el esquema (82) propuesto en Shavitt & Karplus, pag.414. 

function [fxr]=m11_m11t(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
global intqrec
puntos=40;
nmax=10;
%puntos=24;
%nmax=5;
limy=[0;nmax*pi];
limx=[0;1];
a=b1-b2;
a=norm(a);
intrec=0;
fxr=intdobleg('fu11_11t',[limx;limy],puntos,...
    a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
fxr=32*(2*pi)^(0.5)*(alfa1+alfa2)*beta1*beta2*fxr*a^5;
fxr=fxr*((alfa1*alfa2*beta1*beta2)^(1.5))/(pi)^2; 
return   

   
