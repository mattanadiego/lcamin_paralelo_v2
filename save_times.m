function [] = save_times(mdatos,comp_v_time,lcaomain_time,mol_total_energy,my_rank,seed,num_procs)

data_path = getenv("LCAOMINPATH");
data_path = strcat(data_path, 'agua/');

time_file = strcat(data_path, mdatos);
time_file = strcat(time_file, '_');
procs_s = num2str(num_procs);
time_file = strcat(time_file, procs_s);
time_file = strcat(time_file, 'procs_times');
rank_s = num2str(my_rank);
time_file = strcat(time_file, rank_s);

datos = [];

disp('Guardando tiempos...')
disp(' ')
%disp('Comp_v_time:')
%comp_v_time
%disp('lcaomain_time:')
%lcaomain_time
%disp('mol_total_energy:')
%mol_total_energy
datos = [datos;[comp_v_time, lcaomain_time, mol_total_energy]];
save(time_file,'datos','-append');

return;
