function []=savemat(matName,ltype,mat)
 fid   = fopen(matName,"w");
 fwrite(fid,ltype,'int8'); %leemos el primer byte (tipo de dato)
 fwrite(fid,size(mat,1),'int32'); %leemos cantidad de filas
 fwrite(fid,size(mat,2),'int32');
 %leemos cantidad de columnas
 %size(mat,1)
 %size(mat,2)
 %ltype = 0=> entero16, 1=> entero32, 2=> real, 3=>doubble, 
 switch ltype
    case 0
        fwrite(fid,mat,'int16');
    case 1
        fwrite(fid,mat,'int32');
    case 2
        fwrite(fid,mat,'float32');
    case 3 
        fwrite(fid,mat,'float64');
 endswitch

 fclose(fid);
 
return 
