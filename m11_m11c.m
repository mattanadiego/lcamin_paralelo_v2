% Esta rutina eval�a las integrales cuatric�ntricas (1sa 1sb|1sc 1sd)
% seg�n el esquema (79) propuesto en Shavitt & Karplus, pag.413. 
function [fxr]=m11_m11c(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
global intqrec
if (any(a1-a2)&& any(b1-b2))
   puntos=32;
   limx=[0;1];
   limy=[0;1];
   limz=[0;20];
   a=a1-a2;
   a=norm(a);
   c=b1-b2;
   c=norm(c);
   intrec=0;
   fxr=inttripleg('fu11_11c',[limx;limy;limz],puntos,...
      a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   fxr=(4/pi)^(2)*(alfa1*alfa2*beta1*beta2)^(2.5)*a^5*c^5*fxr;
end
return

function recorte
if (~any(a1-a2)&& ~any(b1-b2))
   taua=(alfa1-alfa2)/(alfa1+alfa2);
   taub=(beta1-beta2)/(beta1+beta2);
   tau=(taua-taub)/(taua+taub);
   pseda=0.5*(taua+taub);
   fxr=((1+taua)*(1-taua)*(1+taub)*(1-taub))^1.5;
   fxr=(1/8)*fxr*(1-tau^2)*(5-tau^2)*pseda;
end
return
