% ------------------------------------------------------   
% Esta funci�n describe el integrando de la funci�n m11_m11t

function [fu]=fu11_11t(pares,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)   
u1=pares(:,1);
u2=pares(:,2);
a11=b1-b2;
a11=norm(a11);
%jorge en acci�n z1=beta1^2+(beta2^2-beta1^2)*u1;
z1=beta1^2+(beta2^2-beta1^2)*u1;
% jorge en acci�n pp1=(u1*b1'+(1-u1)*b2'-ones(length(u1),1)*a1')';
pp1=(u1*b1'+(1-u1)*b2'-ones(length(u1),1)*a1')';
pp1=sqrt(sum(pp1.*pp1))';
z1=(z1*ones(1,size(u2,2)))+...
   ((u1.*(1-u1))*ones(1,size(u2,2))).*...
   (u2./(pp1*ones(1,size(u2,2)))).^2;
z1=sqrt(z1)*a11;
fu=(((u1.*(1-u1))*ones(1,size(u2,2))).*(sin(u2)./u2)).*...
   k1(z1).*...
   (1./((alfa1+alfa2)^2+(u2./(pp1*ones(1,size(u2,2)))).^2).^2);
fu=fu./(pp1*ones(1,size(fu,2)));
fu=fu';
return

% ------------------------------------------------------   
function [k11]=k1(q)
	k11=q.^(-5)*(pi/2)^(0.5).*exp(-q);
	k11=k11.*(3+3*q+q.^2);
return

% ------------------------------------------------------   
function [p11]=p1(u,a1,a2,a3);
 	p11=u*a1+(1-u)*a2-a3;
  	p11=(p11'*p11)^(0.5);
return
     

