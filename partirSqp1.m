function[] = partirSqp1(sqp1,nproc)
    workers = nproc - 1;
    filas = size(sqp1,1);

    for j = 1 : workers
        mat = [];
        dh = j;
        mat = sqp1(j:j,:);
        while (dh <= filas)
            dh = dh + workers;
            if (dh <= filas)
                mat = [mat;sqp1(dh:dh,:)];
            end
        end
        name = strcat('sqp1_',num2str(j));
        savemat(name,0,mat);
    end
end
