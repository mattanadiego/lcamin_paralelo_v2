% Esta funci�n reordena las columnas de md para ubicar al principio
% todos los orbitales esponenciales y a continuaci�n los gaussianos.
% Agrga una �ltima fila a md que indica el reordenamiento realizado. 

function [md]=get_dat(md);
md=md(1:5,:);
md=[md;[1:1:size(md,2)]];  
rd1=find(md(5,:)==1);
rd2=find(md(5,:)==2);
if ~isempty(rd1) && ~isempty(rd2)
   md=md(:,[rd1 rd2]);
end
return
