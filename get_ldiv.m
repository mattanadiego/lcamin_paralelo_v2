function [ldiv]=get_ldiv(md)
    ldiv=md(5,:)== 1;
    ldiv = sum(ldiv');
return
