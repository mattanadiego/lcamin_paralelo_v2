function [fxi]=inttripleg(F,lim,puntos,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20
global intq24 intq32 intq40 intq60 intq80 intq100 intq120
global terxyz24
if size(lim,1)==6
   limx=lim(1:2,:);
   limy=lim(3:4,:);
   limz=lim(5:6,:);
else
   limx=lim;
   limy=lim;
   limz=lim;
end

if isempty(puntos)
   puntos=24
end

if length(puntos)==1
   puntos=repmat(puntos,3,1);
end

for i=1:3
   switch puntos(i)
   case 2
      intq=intq2;
   case 3
      intq=intq3;
   case 4
      intq=intq4;
   case 6
      intq=intq6;
   case 8
      intq=intq8;
   case 12
      intq=intq12;
   case 16
      intq=intq16;
   case 20
      intq=intq20;
   case 24
      intq=intq24;
   case 32
      intq=intq32;
   case 40
      intq=intq40;
   case 60
      intq=intq60;
   case 80
      intq=intq80;
   case 100
      intq=intq100;
   case 120
      intq=intq120;
   otherwise
      intq=intq12;
   end
   switch i
   case 1
      intqx=intq;
   case 2
      intqy=intq;
   case 3
      intqz=intq;
   end
end

ex = intqx(:,1);
wx = intqx(:,2);
xs=limx(2);
xi=limx(1);
diffx = xs' - xi';
xcx = 0.5*( (xi'+xs')+(diffx .* ex'));
lxcx=length(xcx);

ey = intqy(:,1);
wy = intqy(:,2);
ys=limy(2);
yi=limy(1);
diffy = ys' - yi';
ycy = 0.5*( (yi'+ys')+(diffy .* ey'));
lycy=length(ycy);

ez = intqz(:,1);
wz = intqz(:,2);
zs=limz(2);
zi=limz(1);
diffz = zs' - zi';
zcz = 0.5*( (zi'+zs')+(diffz .* ez'));
lzcz=length(zcz);

% Contruyo las ternas (x,y,z)
parxy=repmat(xcx',lycy,1);
parxy=[parxy,reshape(repmat(ycy,lxcx,1),lxcx*lycy,1)];
pesxy=reshape(wx*wy',lxcx*lycy,1);
lxycxy=size(parxy,1);
parxyz=repmat(parxy,lzcz,1);
parxyz=[parxyz,reshape(repmat(zcz,lxycxy,1),lxycxy*lzcz,1)];
pesxyz=reshape(pesxy*wz',lxycxy*lzcz,1);

[fx]=feval(F,parxyz,varargin{:});
fxi=(1/8)*(diffx*diffy*diffz)*(fx*pesxyz);
return

   
