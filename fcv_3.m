% Funcion integrada en cv_3, utilizada en calc_va


function [yy,yyi]=fcv_3(x,a1,alfa1,b1,beta1,r)
	x=x';
	u1= cv(x,a1,alfa1,b1,beta1,r);
   u2= (beta1*alfa1^2)./(alfa1^2+4*beta1*x.^2);
   u3= tv(x,a1,alfa1,b1,beta1,r);
	norma=(a1-b1)'*(a1-b1);
	if all(abs(u3))<= 1e-16;
    	if all(a1==0) && all(b1==0) && all(r==0);
      	yy=exp(-x.^2).*u1;
   	else
         yy=exp(-x.^2).*u1.*exp(-u2*norma);
      end
   else
      yy=exp(-x.^2).* u1.* exp(-u2*norma);
      yy=yy.*(pi./u3).^.5;
      yy=yy.* erf(u3.^.5)*.5;
   end
   yy=yy';
   yyi=[];
return


function [u1]= cv(x,a1,alfa1,b1,beta1,r);
    cv1=8*pi*x.^2;
    cv2=(alfa1^2+4*beta1*x.^2);
    u1=cv1./cv2;
return


function [u3]=tv(x,a1,alfa1,b1,beta1,r)
	u3=1./(4*x.^2);
	tt=(alfa1^2+4*beta1*x.^2);
	u3=u3./tt;
	da=a1-r;
	db=b1-r;
	aa=alfa1^2*da(1)+4*beta1*x.^2*db(1);
	bb=alfa1^2*da(2)+4*beta1*x.^2*db(2);
	cc=alfa1^2*da(3)+4*beta1*x.^2*db(3);
	nn=aa.*aa + bb.*bb + cc.*cc;
	u3=u3.*nn;
return


