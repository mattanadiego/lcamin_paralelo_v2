function globales
global globales_intq_path 

%globales_intq_path='/home/tardivo/investigacion/lcaomin_jor/auxiliares/';
lcaomipath=getenv("LCAOMINPATH");
globales_intq_path=strcat(lcaomipath,'auxiliares/');

%Matriz con los ceros de las funciónes de Bessel
global lambda
fin=fopen(strcat(globales_intq_path,'lambda.bin'),'r');
lambda=fread(fin,'double');
lambda = reshape(lambda,62,200);

%Parametros globales para la rutinas de integracion intquad2
global intq2 intq3 intq6 intq8 intq12 intq16 intq20 
global intq24 intq32 intq40 intq60 intq80 intq100 intq120
global intrec
intrec=0;

%Parametros globales paralas rutinas de integracion
%calc_2
global puntos2 li2 ls2
puntos2=40;		li2 = 0;			ls2=1;
%calc_3
global puntos3 li3 ls3
puntos3=40;   	li3 = 0;			ls3=10;
%comp_sa
global puntoss3 lis3 lss3
puntoss3=40;   	lis3 = 0;			lss3=20;
%comp_ta
global puntost3 lit3 lst3
puntost3=40;   	lit3 = 0;			lst3=50;
%comp_va
%cv_2
global puntosv2 lxv2 lyv2 lzv2
puntosv2=40;   	lxv2 = [0;2*pi];  	lyv2=[-1; 1];	lzv2=[0; 15];
%cv_3
global puntosv3 liv3 lsv3
puntosv3=40;   	liv3 = 0;			lsv3=500;
%cv_4
global puntosv4 liv4 lsv4
puntosv4=40;   	liv4 = 0;			lsv4=1;

% Parámetros globales para la rutina método
global tolmet
tolmet=1e-7;

clc
fin=fopen(strcat(globales_intq_path,'intq2.bin'),'r');
intq2=fread(fin,'double');
intq2=reshape(intq2,2,2);
fin=fopen(strcat(globales_intq_path,'intq3.bin'),'r');
intq3=fread(fin,'double');
intq3=reshape(intq3,3,2);
fin=fopen(strcat(globales_intq_path,'intq4.bin'),'r');
intq4=fread(fin,'double');
intq4=reshape(intq4,4,2);
fin=fopen(strcat(globales_intq_path,'intq6.bin'),'r');
intq6=fread(fin,'double');
intq6=reshape(intq6,6,2);
fin=fopen(strcat(globales_intq_path,'intq8.bin'),'r');
intq8=fread(fin,'double');
intq8=reshape(intq8,8,2);
fin=fopen(strcat(globales_intq_path,'intq12.bin'),'r');
intq12=fread(fin,'double');
intq12=reshape(intq12,12,2);
fin=fopen(strcat(globales_intq_path,'intq16.bin'),'r');
intq16=fread(fin,'double');
intq16=reshape(intq16,16,2);
fin=fopen(strcat(globales_intq_path,'intq20.bin'),'r');
intq20=fread(fin,'double');
intq20=reshape(intq20,20,2);
fin=fopen(strcat(globales_intq_path,'intq24.bin'),'r');
intq24=fread(fin,'double');
intq24=reshape(intq24,24,2);
fin=fopen(strcat(globales_intq_path,'intq32.bin'),'r');
intq32=fread(fin,'double');
intq32=reshape(intq32,32,2);
fin=fopen(strcat(globales_intq_path,'intq40.bin'),'r');
intq40=fread(fin,'double');
intq40=reshape(intq40,40,2);
fin=fopen(strcat(globales_intq_path,'intq60.bin'),'r');
intq60=fread(fin,'double');
intq60=reshape(intq60,60,2);
fin=fopen(strcat(globales_intq_path,'intq80.bin'),'r');
intq80=fread(fin,'double');
intq80=reshape(intq80,80,2);
fin=fopen(strcat(globales_intq_path,'intq100.bin'),'r');
intq100=fread(fin,'double');
intq100=reshape(intq100,100,2);
fin=fopen(strcat(globales_intq_path,'intq120.bin'),'r');
intq120=fread(fin,'double');
intq120=reshape(intq120,120,2);

%Ternas para integracion triple
fin=fopen(strcat(globales_intq_path,'terxyz24.bin'),'r');
terxyz24=fread(fin,'double');
terxyz24=reshape(terxyz24,13824,4);

%Pares para integracion doble
fin=fopen(strcat(globales_intq_path,'terxy40.bin'),'r');
terxy40=fread(fin,'double');
terxy40=reshape(terxy40,1600,3);


return
