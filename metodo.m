% Control para S inicial
% ind=1, selecciona XC
% ind=2, selecciona XS. Defecto 
% ind=3, selecciona XI

% Control para P inicial
% indp=0, selecciona ceros
% indp=1, selecciona identidad
% indp=2, selecciona atovec. S
% indp=3, selecciona por fase I. Defecto

function [energy,autoval,cs,pse,ctrl,s0]=metodo(vta,vva,vsa,...
   mat1,mat2,sz,sqp1,clsmd,tolmet,ind,indp)
%global mat
mat=mat1-0.5*mat2;
switch nargin
case 8
   tolmet=1e-6;
   ind=2;
   indp=3;
case 9
   ind=2;
   indp=3;
end
vta = get_low(vta,sqp1,clsmd);
vva = get_low(vva,sqp1,clsmd);
vsa = get_low(vsa,sqp1,clsmd);
h = vta-vva;
h = xpnd(h);
s = xpnd(vsa);
t = xpnd(vta);
v = xpnd(vva);
ht=h+t;
save matrices h s t v ht
H = reshape(h,size(h,1)*size(h,2),1);
vsz = [1:1:sz];
% ----------- CALCULO DE LA MATRIZ P ------------------ */
s0=s;
[XS,XC,XI,U,lam]=calc_X(s0);
[H1,H2]=calc_HE(h,s,t,v);
if isempty(tolmet)
   tolmet =1e-5;
end
per=[1:clsmd]';
[pse0,pse1,pse2]=calc_Pini(U,lam,clsmd);

switch ind
case 1
   s=XC; 		%ind=1
case 2
   s=XS; 		%ind=2
case 3
   s=XI; 		%ind=3
otherwise
   s=XC;
end


[pse3,energy,cs,fs,autoval,ctrl]=calc_C(s,s0,mat,H1,pse0,...
   vsz,clsmd,sqp1,per,tolmet,2);

switch indp
case 0
   psein=pse0;
case 1
   psein=pse1;
case 2
   psein=pse2;
case 3
   psein=pse3;
otherwise
   psein=pse3;
end
%[pse13,energy13,cs13,fs13,autoval13,ctrl13]=calc_C(XC,s0,mat,H,psein,...
%   vsz,clsmd,sqp1,per,tolmet,1);
[pse23,energy23,cs23,fs23,autoval23,ctrl23]=calc_C(s,s0,mat,H,psein,...
   vsz,clsmd,sqp1,per,tolmet,ind);
%[pse33,energy33,cs33,fs33,autoval33,ctrl33]=calc_C(XI,s0,mat,H,psein,...
%   vsz,clsmd,sqp1,per,tolmet,3);

pse=pse23;
energy=energy23;
cs=cs23;
fs=fs23;
autoval=autoval23;
control=ctrl23;
%VER1=verif_PAV(fs13,cs13,autoval13,s0);
VER2=verif_PAV(fs23,cs23,autoval23,s0);
%VER3=verif_PAV(fs33,cs33,autoval33,s0);
return

function [pse,energy,cs,fs0,autoval,ctrl]=calc_C(s,s0,mat,...
   H,pse,vsz,clsmd,sqp1,per,tolmet,ind)
energy = 1000;
err1 = 10;
iter = 0;
ctrl = 0;
energy0=1000;
aut=[];
rper=[];
itermax=300;
[fs,energy,vas] = calc_ener(pse,mat,H,clsmd);
while err1 > tolmet
   if iter>itermax
      ctrl=1;
      break
   end
   fs = reshape(fs,clsmd,clsmd);
   fs=0.5*(fs+fs');
   switch ind
   case 1
      frs=s'*fs*s;
      frs=0.5*(frs+frs');
   case 2
      frs=s*fs*s;
      frs=0.5*(frs+frs');
   case 3
      frs=s*fs;
   end
        
   [cs,autoval]=eig(frs);
   [autoval,per]=sort(diag(autoval));
   cs=cs(:,per);
   if (ind~=3)
      cs=s*cs;
   end
            
   if size(autoval,2)>1
      autoval=diag(autoval);
   end
   aut=[aut;[autoval' energy]];
   rper=[rper;per'];
   save niter aut rper
      
   den=1./sqrt(diag(cs'*s0*cs));
   den=repmat(den',length(den),1);
   cs=cs.*den;
   %cs0=cs;
   pse=calc_p(cs,vsz);
   %pse0=pse;
   fs0=fs;
   [fs,energy,vas] = calc_ener(pse,mat,H,clsmd);
   if energy0
      err1=abs(energy-energy0)/abs(energy0); 
   else
      err1=abs(energy-energy0);
   end
   energy0=energy;
   iter = iter + 1;
end
return

function [XS,XC,XI,U,lam]=calc_X(s)
[U,lam]=eig(s);
den=1./sqrt(diag(lam));
den=repmat(den',length(den),1);
XC=U.*den;
XS=XC*U';
XI=XC*XC';
return

function [H1,H2]=calc_HE(h,s,t,v)
ha=diag(h);
hab=ha*ones(1,length(ha))+ones(length(ha),1)*ha';
h1=s.*hab;
H1=reshape(h1,size(h1,1)*size(h1,2),1);
va=diag(v);
vab=va*ones(1,length(va))+ones(length(va),1)*va';
h2=t+0.5*s.*vab;
H2=reshape(h2,size(h2,1)*size(h2,2),1);
return


% ====================================================== */
% Calculo de F=H+sum(P*mat)
function [F,energy,vas]=calc_ener(P,mat,H,clsmd);

[rpse,cpse]=size(P);
lpse=rpse*cpse; 
P= reshape(P,lpse,1);
vas=zeros(lpse,1);
for i=1:lpse
   vas=vas+P(i)*mat((i-1)*lpse+1:i*lpse);
end
F= H + vas;
energy = P .* (F + H);
energy = 0.5 * sum(energy);
vas=reshape(vas,clsmd,clsmd);
return

% ===================================================== */
% C�lculo de la matriz P
function [P]=calc_p(cr1,sz)
cr1=cr1(:,sz);
P=2*cr1*cr1';
save Matrizp P
return

% ====================================================== */
function [mv1]=get_low(mv,sqp1,clsmd)
mv1 = sortc([mv sqp1],3);
for i=1:clsmd
   ei= find(mv1(:,3)==i);
   mv1(ei,:) = sortc(mv1(ei,:),2);
end
mv1=mv1(:,1);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUTINAS VIEJAS
function [energy,autoval,cs,pse,iter,ctr]=metodooc(vta,vva,vsa,sz,...
															sqp1,sqp2,clsmd,nomarch)
global tolmet mat
%load matnew                                          
%mat = get_mat(vclmn,indices);
vta = get_low(vta,sqp1,clsmd);
vva = get_low(vva,sqp1,clsmd);
vsa = get_low(vsa,sqp1,clsmd);
h= vta-vva;
h = xpnd(h);
s = xpnd(vsa);
t = xpnd(vta);
ht=h+t;
save matrices h s t ht
H = reshape(h,size(h,1)*size(h,2),1);
vsz = [1:1:sz];

% ----------- CALCULO DE LA MATRIZ P ------------------ */
cs0=zeros(clsmd,clsmd);
energy = 1000;
pse=cs0;
psF=cs0;
%load matnew
[t,aut]=eig(s);
S1=t*diag(1./diag(aut))*t'; %Inv(s)
[aut,per]=sort(diag(aut));
t=t(:,per);
aut=sqrt(aut);
if any(aut)==0
   iaut0=find(aut>0);
   aut(iaut0)=1./aut(iaut0);
end
aut=1./aut;
aut=repmat(aut',length(aut),1);
rss=t.*aut;
save rss rss t
err1 = 10;
iter = 0;
ctr = 0;
energy0=1000;
if isempty(tolmet)
   tolmet =1e-5;
end
%%%%%%%%%%%%
aut=[];
ener=[];
%clc
while err1 > tolmet
   if iter>100
      ctr=1;
      break
   end
   [fs,energy] = calc_ener(real(pse),mat,H);
   fs = reshape(fs,clsmd,clsmd);
   fs = fs';
   frs=rss' * fs * rss;
   frs=0.5*(frs+frs');
   [crs,autoval]=eig(frs);
   [autoval,per]=sort(diag(autoval));
   [fsF,energy] = calc_ener(real(psF),mat,H);
   fsF = reshape(fsF,clsmd,clsmd);
   SF=S1*fsF;
   [cF,AF]=eig(SF);
   [AF,perF]=sort(diag(AF));
   cF=cF(:,perF);

   %%%%%%%%%%%%%%%%%%%
   aut=[aut autoval];
   ener=[ener;energy];
   save niter aut ener
   %%%%%%%%%%%%%%%%%%%
   crs=crs(:,per);
   cs=rss * crs;
   format short
   disp('cs')
   disp(cs);
   err2=max(max(abs(cs-cs0)));
   if energy0
      err1=abs(energy-energy0)/abs(energy0); 
   else
      err1=abs(energy-energy0);
   end
   energy0=energy;
   cs0=cs;
   pse=calc_p(cs,vsz,sqp1,clsmd);
   psF=calc_p(cF,vsz,sqp1,clsmd);
   
   disp('pse')
   disp(pse)
   iter = iter + 1;
end
vsz1 = [1:1:sz];
pse=calc_p(cs,vsz1,sqp1,clsmd);
[fs1,energy1] = calc_ener(pse,mat,H);
return

% C�lculo de la matriz P
function [P]=calc_pold(cr1,sz,sqp1,clsmd)
cr1=cr1(:,sz);
P=zeros(size(sqp1,1),1);
par1=1;
for par1=1:size(sqp1,1);
    mu=sqp1(par1,1);
    nu=sqp1(par1,2);
    P(par1)=2*cr1(mu,:) * cr1(nu,:)';
end
P = [P sqp1];
P = get_low(P,sqp1,clsmd);
P = xpnd(P(:,1));
return

function [pse0,pse1,pse2]=calc_Pini(U,lam,clsmd)
pse0=zeros(clsmd);
pse1=eye(clsmd);
ilam=diag(1./sqrt(diag(lam)));
cs1=U*ilam;
pse2=2*cs1(:,1:2)*cs1(:,1:2)';
return

function [VER]=verif_PAV(fs,cs,autoval,s0)
VAI=fs*cs;
VAD=s0*cs*diag(autoval);
VER=max(max(abs(VAI-VAD)));
return
