function [mlmn]=sacamat(nomarch,i,ns,ls)
global sqp1
	[arch]=nom_arch(nomarch,i);
   fin=fopen(arch,'r');
   if i<4
      mlmn=fread(fin,'double');
   elseif i==4
      mlmn=fread(fin,'short');
   end
   rlen = (ns * ((ls+1) * (ls+2))/2);
   if ~isempty(mlmn)
      mlmn=reshape(mlmn,rlen,length(mlmn)/rlen);
   end
return

