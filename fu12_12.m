% Funcion que se integra en la rutina m11_22.
function [fu]=fu12_12(parxy,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
u1=parxy(:,1);
u2=parxy(:,2);
[t,sx,sy,sxy]=te_12(u1,u2,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
f0=efe0(t);
ra1a2=(a1-a2)'*(a1-a2);
rb1b2=(b1-b2)'*(b1-b2);
d1=(alfa1^2*alfa2)./sx;
d2=(beta1^2*beta2)./sy;
num=exp(-u1.^2-u2.^2);
num=num.*(u1.^3).*(u2.^3);
num=num.*exp(-d1*ra1a2-d2*rb1b2);
den=sx.*sy.*sqrt(sxy);
fu=num./den;
fu=(fu.*f0)';
return

% ================================================
function [s]=suma(u,alfa1,alfa2);
	s=alfa1^2+4*u.^2*alfa2;
return

% ================================================
% Procedimiento que genera la combinacion convexa*/
function [y]=pconv(u,a1,alfa1,a2,alfa2)
	y=zeros(size(u,1),3);
  	y(:,1)=alfa1^2*a1(1)+4*alfa2*u.^2*a2(1);
  	y(:,2)=alfa1^2*a1(2)+4*alfa2*u.^2*a2(2);
  	y(:,3)=alfa1^2*a1(3)+4*alfa2*u.^2*a2(3);
  	yd=suma(u,alfa1,alfa2);
  	y=y./(yd*ones(1,3));
return

% ================================================
% Procedimiento que genera el t de la funcion f0
function [t,t1,t2,tt1]=te_12(u1,u2,a1,alfa1,a2,...
alfa2,b1,beta1,b2,beta2)
t1=suma(u1,alfa1,alfa2);	
t2=suma(u2,beta1,beta2);	
tt1=(alfa1^2*u2.^2+beta1^2*u1.^2)+...
   4*(alfa2+beta2)*(u1.^2).*(u2.^2);
t=t1.*t2;
t=t./(4*tt1);
y1=pconv(u1,a1,alfa1,a2,alfa2);
y2=pconv(u2,b1,beta1,b2,beta2);
yy=y1-y2;
norma=sum((yy.*yy)')';
t=t.*norma;
return
