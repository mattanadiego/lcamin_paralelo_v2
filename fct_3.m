
% Funcion integrada en ct_3, utilizada en calc_ta por calc_t

function [yyr,yyi]=fct_3(x,a1,alfa1,b1,beta1,norma2)
    f1=((x.^3) ./ ((4*beta1*x.^2+alfa1^2).^3.5));
    f2= exp((-x.^2)-(beta1* alfa1^2 * norma2)./(4*beta1*x.^2+alfa1^2));
    f3= 24*beta1*x.^4+6*alfa1^2*x.^2+alfa1^4*norma2;
    yyr=f1.*f2.*f3;
    yyi=[];
return
