function [mat]=reshapeSqp1(sqp1)
    j=3; %para avanzar i la primera vez
    i=0;
    aq =0;

    for k= 1:length(sqp1)
        
        if str2num(sqp1(k))>=0 & str2num(sqp1(k))<=9
            aq= aq*10+str2num(sqp1(k));
        else
            if aq>0            
                j = j+1;
                if j>2
                    i=i+1;
                    j=1;
                end
                
                mat(i,j)=aq;
                aq =0;
            end
        end
    endfor
    if aq>0
        j = j+1;
        if j>2
            i=i+1;
            j=1;
        end
        mat(i,j)=aq;
    end

return
