
function [fxi]=intdoble(F,limx,puntosx,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20
global intq24 intq32 intq40 intq60 intq80 intq100 intq120
global terxy40
if isempty(puntosx)
   puntosx=40
end

switch puntosx
case 2
   intqx=intq2;
case 3
   intqx=intq3;
case 4
   intqx=intq4;
case 6
   intqx=intq6;
case 8
   intqx=intq8;
case 12
   intqx=intq12;
case 16
   intqx=intq16;
case 20
   intqx=intq20;
case 24
   intqx=intq24;
case 32
   intqx=intq32;
case 40
   intqx=intq40;
case 60
   intqx=intq60;
case 80
   intqx=intq80;
case 100
   intqx=intq100;
case 120
   intqx=intq120;
otherwise
   intqx=intq12;
end

ex = intqx(:,1);
wx = intqx(:,2);
xs=limx(2);
xi=limx(1);
diffx = xs' - xi';
xcx = 0.5*( (xi'+xs')+(diffx .* ex'));
if (~isempty(terxy40)&puntosx==40)
   parxy=terxy40(:,1:2);
   pesxy=terxy40(:,3);
else
   % Contruyo los pares (x,y)
   %parxy=repmat(xcx',lxcx,1);
   %parxy=[parxy,reshape(repmat(xcx,1,lxcx),lxcx^2,1)];
   %pesxy=reshape(wx*wx',lxcx^2,1);
   parxy=[];
   pesxy=[];
   lxcx=length(xcx);
   for j=1:lxcx
      parxy=[parxy;[xcx' ones(lxcx,1)*xcx(j)]];
      pesxy=[pesxy;wx*wx(j)];
   end
end
[fx]=feval(F,parxy,varargin{:});
fxi=(1/4)*(diffx.^2)*(fx*pesxy);
return

   

