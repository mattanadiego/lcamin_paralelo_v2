function [fx]=int11_11cma(F,limx,limr,puntosx,puntosr,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20
global intq24 intq32 intq40 intq60 intq80 intq100 intq120

switch puntosx
case 2
   intqx=intq2;
case 3
   intqx=intq3;
case 4
   intqx=intq4;
case 6
   intqx=intq6;
case 8
   intqx=intq8;
case 12
   intqx=intq12;
case 16
   intqx=intq16;
case 20
   intqx=intq20;
case 24
   intqx=intq24;
case 32
   intqx=intq32;
case 40
   intqx=intq40;
case 60
   intqx=intq60;
case 80
   intqx=intq80;
case 100
   intqx=intq100;
case 120
   intqx=intq120;
otherwise
   intqx=intq12;
end

ex = intqx(:,1);
wx = intqx(:,2);
xs=limx(2);
xi=limx(1);
diffx = xs' - xi';
xcx = 0.5*( (xi'+xs')+(diffx .* ex'));

switch puntosr
case 2
   intqr=intq2;
case 3
   intqr=intq3;
case 4
   intqr=intq4;
case 6
   intqr=intq6;
case 8
   intqr=intq8;
case 12
   intqr=intq12;
case 16
   intqr=intq16;
case 20
   intqr=intq20;
case 24
   intqr=intq24;
case 32
   intqr=intq32;
case 40
   intqr=intq40;
case 60
   intqr=intq60;
case 80
   intqr=intq80;
case 100
   intqr=intq100;
case 120
   intqr=intq120;
otherwise
   intqr=intq12;
end

er = intqr(:,1);
wr = intqr(:,2);
xsr=limr(2);
xir=limr(1);
diffr = xsr' - xir';
xcr = 0.5*( (xir'+xsr')+(diffr .* er'));

% Contruyo los pares (u,r)
parxr=[];
pesow=[];
lxcx=length(xcx);
lxcr=length(xcr);
for j=1:lxcr
   parxr=[parxr;[xcx' ones(lxcx,1)*xcr(j)]];
   pesow=[pesow;wx];
end

[p1]=pm(xcx,wx,varargin{1},varargin{3},varargin{5},varargin{7});
u0=0.4;
[u0,v0]=apm(u0,p1,varargin{1},varargin{3},varargin{5},varargin{7});
save p1 p1 u0 v0
[ut,vt,f0,Dfu,Dfv,Dfu2,Dfv2,Dfuv,uu0,vv0]=feval(F,parxr,pesow,xcr,p1,u0,v0,varargin{:});
fx=0;
for j=1:lxcr
   fx0=sum(sum(ut((j-1)*lxcx+1:j*lxcx)*vt((j-1)*lxcx+1:j*lxcx)'));
   fx0=fx0*f0(j);
   fxu=0;
   fxv=0;
   fxu2=0;
   fxuv=0;
   fxv2=0;
   if varargin{9}>=2
      fxu=sum(sum((ut((j-1)*lxcx+1:j*lxcx).*uu0((j-1)*lxcx+1:j*lxcx))...
         *vt((j-1)*lxcx+1:j*lxcx)'));
      fxu=fxu*Dfu(j);
      fxv=sum(sum(ut((j-1)*lxcx+1:j*lxcx)*(vt((j-1)*lxcx+1:j*lxcx)...
         .*vv0((j-1)*lxcx+1:j*lxcx))'));
      fxv=fxv*Dfv(j);
   end
   if varargin{9}>=3
      fxu2=sum(sum((ut((j-1)*lxcx+1:j*lxcx).*uu0((j-1)*lxcx+1:j*lxcx).^2)...
         *vt((j-1)*lxcx+1:j*lxcx)'));
      fxu2=fxu2*Dfu2(j);
      fxuv=sum(sum((ut((j-1)*lxcx+1:j*lxcx).*uu0((j-1)*lxcx+1:j*lxcx))...
         *(vt((j-1)*lxcx+1:j*lxcx).*vv0((j-1)*lxcx+1:j*lxcx))'));
      fxuv=fxuv*Dfuv(j);
      fxv2=sum(sum(ut((j-1)*lxcx+1:j*lxcx)*(vt((j-1)*lxcx+1:j*lxcx)...
         .*vv0((j-1)*lxcx+1:j*lxcx).^2)'));
      fxv2=fxv2*Dfv2(j);
   end
   fxi=(1/8)*diffr.*(diffx.^2).*(fx0+fxu+fxv+0.5*(fxu2+2*fxuv+fxv2)).*wr(j);
   fx=fx+fxi;
end
return

function [p1]=pm(xcx,wx,a1,a2,a3,a4)
lxcx=length(xcx);
parxx=[];
pesow=[];
for j=1:lxcx
   parxx=[parxx;[xcx' ones(lxcx,1)*xcx(j)]];
   pesow=[pesow;wx.*(wx(j)*ones(lxcx,1))];
end
u=parxx(:,1);
v=parxx(:,2);
px=(u*a2(1)+(1-u)*a1(1))-(v*a4(1)+...
   (1-v)*a3(1))*ones(1,size(u,2));
py=(u*a2(2)+(1-u)*a1(2))-(v*a4(2)+...
   (1-v)*a3(2))*ones(1,size(u,2));
pz=(u*a2(3)+(1-u)*a1(3))-(v*a4(3)+...
   (1-v)*a3(3))*ones(1,size(u,2));
p1=sqrt((px.*px+py.*py+pz.*pz));
p1=0.25*sum(p1.*pesow);
return

function [u0,v0]=apm(u,pm,a1,a2,a3,a4)
u0=u;
c1=u*(a2-a1)+a1-a3;
c2=(a3-a4);
a=c2'*c2;
b=c1'*c2;
c=c1'*c1-pm^2;
u=0.1;
while b^2-a*c<0
   c1=u*(a2-a1)+a1-a3;
   b=c1'*c2;
   c=c1'*c1-pm^2;
   u=u+0.1;
end
v0p=(-b+sqrt(b^2-a*c))/a;
v0n=(-b-sqrt(b^2-a*c))/a;
if ((v0n<0)|(v0n>1))
   v0=v0p;
else
   v0=v0n;
end
return

