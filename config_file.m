%
% Configuration file
%
function [comp_4in_t,comp_v_t,lcaomain_t,mol_total_e]=config_file()

% Define global variable
global comp_4in_time comp_v_time lcaomain_time mol_total_energy;

comp_4in_t = comp_4in_time;
comp_v_t = comp_v_time;
lcaomain_t = lcaomain_time;
mol_total_e = mol_total_energy;

% Path Cluster
setenv('LCAOMINPATH', '/home/ltardivo/lcaomin_paralelo_v2/');

return;
