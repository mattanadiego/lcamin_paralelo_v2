function contr_mat(matname,vsf,mat1,mat2)
mat=mat1-0.5*mat2;
mata=mat;
vsfa=vsf;
load(matname)
if (length(vclmn)==length(vsfa))
   rvsf=[vclmn vsfa];
   rmat=[mat mata];
   matname=strcat('R',matname);
   save(matname,'rvsf','rmat');
end
return
