% Calculo de las cantidades de 4 indices utilizando los coeficientes
% Clmn.
function [fxr]=fourind(a1,alfa1,a2,alfa2,...
   b1,beta1,b2,beta2,par1,par2,sqp1)
global fourind_path ns ls ldiv radio
ns=20;
ls=20;
radio=5;
	clc
   tol=1e-15;
   ctrlm1 = control(a1,a2);
   ctrlm2 = control(b1,b2);
   ctrlcp1 = control2(sqp1,par1,ldiv);
   ctrlcp2 = control2(sqp1,par2,ldiv);
   [vclmnp1,ulmn1,ulmn2]=calc_00(a1,alfa1,a2,alfa2,ctrlcp1,...
                           'N',0,0,par1);
   
   [vclmnp2,ulmn1,ulmn2]=calc_00(b1,beta1,b2,beta2,ctrlcp2,...
                           'N',0,0,par2);
   if ~ctrlm1
      [clmnp1,ulmn1,ulmn2]=calc_11(a1,alfa1,a2,alfa2,...
         ctrlcp1,'N',0,0,par1,ctrlm1);
   else
      clmnp1=zeros(ns*(ls*(ls+1))/2,2);
   end
   if ~ctrlm2
   [clmnp2,ulmn1,ulmn2]=calc_11(b1,beta1,b2,beta2,...
         ctrlcp2,'N',0,0,par2,ctrlm2);
   else
      clmnp2=zeros(ns*(ls*(ls+1))/2,2);
   end
   clmnp1 = sqrt(2)*clmnp1;
   vclmnp1 = [vclmnp1;clmnp1];
   clmnp2 = sqrt(2)*clmnp2;
   vclmnp2 = [vclmnp2;clmnp2];
   c0r1=vclmnp1(:,1);
   c0i1=vclmnp1(:,2);
   c0r2=vclmnp2(:,1);
   c0i2=vclmnp2(:,2);
   fxr=(c0r1' * c0r2 + c0i1' * c0i2) ;
   fxr=(4*pi*radio^2) * fxr;
return

% ===============================================
function [ctrlm]=control(a1,b1);
   tol1=1e-15;
   ctrlm = 0;
   if abs(a1(1)) <= tol1 & abs(a1(2)) <= tol1
      if abs(b1(1)) <= tol1 & abs(b1(2)) <= tol1;
         ctrlm = 1;
      end
   end
return
   


   
