
function [plg]=Plgndr(l,m,x)

    if (m < 0) | (m > l)  | (max(abs(x)) > 1)
      error('Parámetros fuera de rango.')     
    end
    pmm = ones(size(x,1),1);
    if m >= 0;
        somx2=sqrt((1-x) .* (1+x));
        fact=1;
        i=1;
        while i <= m;
            pmm= -(fact * pmm) .* somx2;
            fact=fact+2;
            i=i+1;
        end
     end
     if l == m
        plg=pmm;
     else
        pmmp1=x*(2*m+1) .* pmm;
        if l == m+1;
            plg=pmmp1;
        else;
            ll=m+2;
            while ll <= l;
                pll=(x*(2*ll-1) .* pmmp1 - (ll+m-1) * pmm)/(ll-m);
                pmm=pmmp1;
                pmmp1=pll;
                ll=ll+1;
            end
            plg=pll;
        end

    end
    kte=((2*l+1)*gamma(l-m+1))/(4*pi*gamma(l+m+1));
    plg = sqrt(kte) * plg;
return
%/* ========================================================================== */

function [cotita,fi]=Polcord(p);
    mcc=P;
    rv=[];
    rv2=[];
    cmcc=1;

    while cmcc <= size(mcc,2);
        rv=[rv;sqrt(mcc(:,cmcc)'* mcc(:,cmcc))];
        rv2=[rv2;sqrt(mcc(1:2,cmcc)'* mcc(1:2,cmcc))];
        cmcc=cmcc+1;
    end

    cotita=p(3,:)./RV';
    e0=rv2 ~= 0;
    if sumc(e0) < length(rv2);
        rv2=rv2+(ones(length(rv2),1)-e0);
    end

    fi=p(1,:) ./ RV2';

    cas  = p(2,:) < 0;
    cas1 = 2*pi * cas;
    fi=arccos(fi);
    cas1 = fi + cas1;
    cas1 = cas1 - 2 * fi .* cas;
    fi = cas1;

    % @ Control de p2 = 0 @
    cas  = p(2,:) == 0;
    cas1 = p(1,:) <= 0 ;
    cas = cas .* cas1;
    if sum(cas);
        cas  = find(cas);
        fi(cas) = pi * ones(1,size(cas,1));
    end
    fi = [fi;e0']
return
