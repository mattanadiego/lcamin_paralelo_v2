

%/* ======================  PROCEDIMIENTO UBASE  ========================= */
%/*  Funciones de Modulo 2 contra Funciones de Modulo 2 distintos Indices. */

function [clmnr1, clmni1]=Ubase(rr,n,l,m,radio);
global lambda
rv1=norm(rr);
if rv1
   bv1=besselj(l+0.5,lambda(l+1,n) * (rv1/radio));
   bv1=bv1 ./ sqrt(rv1);
   [cotita,fi]=polcord(rr);
   bv1=bv1 .* plgndr(l,m,cotita');
   cbv=sqrt(2) ./ (radio * besselj(l+0.5,lambda(l+1,n)));
   bv1=cbv .* bv1;
   afi=fi(1,:);
   bvr1= bv1 .* cos(m * afi');             
   bvi1= bv1 .* sin(m * afi');             
   clmnr1= bvr1;
   clmni1= bvi1;
else
   if l
      clmnr1 = 0;
      clmni1 = 0;
   else
      clmnr1 =(-1)^(n+1)* lambda(1,n)/sqrt(2*pi*radio^3);
      clmni1 = 0;
   end
end
return

