/*
TODO: Avanzar en el send de las matrices: particionar las matrices. Usaremos el 
tag para indicar la matriz que mandamos.1=>md, 2=>sqpr1...
*/ 

#include <octave/oct.h>
#include <octave/octave.h>
#include <octave/parse.h>
// Library for old versions of Octave
#include <octave/toplev.h> /* do_octave_atexit */
// Library for new versions of Octave
//#include <interpreter.h>
#include <iostream>
#include "mpi.h"
#include "mpi_broadcast.c"

#define MAX_PARES_SIZE 1000 
int mpi_broadcast(int size);

int main (int argc, char ** argv){

  const char * argvv [] = {"" /* name of program, not relevant */, "--silent"};
      
  octave_main (2, (char **) argvv, true /* embedded */);
 
  octave_value_list functionArguments;
  octave_value_list functionArguments2;
  octave_value_list functionArguments3;
  octave_value_list result_cf;		// config_file
  octave_value_list result_lcao;	// lcao
  octave_value_list result_lcaomain;	// lcaomain
  octave_value_list result_af;		// agua_finalizacion
 
  int my_rank; 
  int nproc;

  /*START MPI */
  MPI_Init(&argc, &argv); 

  /*DETERMINE RANK OF THIS PROCESSOR*/
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 

  /*DETERMINE TOTAL NUMBER OF PROCESSORS*/
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  printf("I'm rank (processor number) %d of size %d\n", my_rank, nproc);

  result_cf = feval ("config_file", functionArguments2, 1);

  /*Espera ocupada por los resultados de los hijos*/
  if (my_rank == 0) {  
    functionArguments (0) = my_rank;
    functionArguments (1) = nproc;
    functionArguments (2) = argv[1];
	
    result_lcaomain = feval ("lcaomain", functionArguments, 1);   
    
    int sal = mpi_broadcast(nproc);
    
    if(sal < 0)
      printf("Error al hacer broadcast \n");

    char OK[3];
    //float lcao_val;
    int i;
    MPI_Status status; 
    //Hacer el receive de las matrices resultado
    for(i = 1; i < nproc; i++) {
      printf("Padre: Esperando hijo %d...\n",i);
      MPI_Recv(OK, 1, MPI_UNSIGNED_CHAR, i, TAG_ALL, MPI_COMM_WORLD, &status); 
    }

    printf("Padre: por combinar matrices\n");
    //Combinar las matrices
    functionArguments (0) = nproc;

    result_af = feval ("agua_finalizacion", functionArguments, 1);

    functionArguments3 (0) = argv[1]; // mdatos
    if (result_cf.length() > 0) {
      if (result_af.length() > 0) {
         functionArguments3 (1) = result_af(0); // comp_v_time
         functionArguments3 (3) = result_af(1); // energy
      }
      if (result_lcaomain.length() > 0)
        functionArguments3 (2) = result_lcaomain(1); // lcaomain_time
      functionArguments3 (4) = my_rank; // rank
      functionArguments3 (5) = argv[2]; // seed
      functionArguments3 (6) = argv[3]; // numbers of processors
    }

    feval ("save_times", functionArguments3, 1);
  } else {
    //hijos: reciben su porción de sqp1 y computan lcao.
    char sqp1[200000]; //= (char * ) malloc(sizeof(char)*MAX_PARES_SIZE); //TODO: cómo calcular este tamaño?
    char md[10000];
    char centros[10000];
    char ldiv[10000];
    int bytes_send;
    char sqp1_name[7];
       
    MPI_Status status; 
    
      /****************************************************************************************************************/
      //Primera recepcion: recibo pares TODO: no se si es correcta hacer esta suposición, porque puede llegar antes md.
      //MPI_Recv(sqp1,200000,MPI_CHAR, 0, TAG_ALL, MPI_COMM_WORLD, &status);
      //MPI_Get_count(&status, MPI_CHAR, &bytes_send);

      //printf("Hijo %d: recibiendo sqp1 \n",my_rank);
      /****************************************************************************************************************/
      //Guardar las matrices a disco para que las levante octave.
       
      /* sprintf(sqp1_name,"sqp1_%d",my_rank);
       printf("POR ABRIR %s \n\n",sqp1_name);
       FILE * sqp1_file = fopen(sqp1_name,"a+");
       if ( sqp1_file == NULL )
       {
         printf("Error al abrir archivo sqp1\n");
         fflush(stdout);
         return -1;
       }
        
       int res = fwrite(pares,1, bytes_send, sqp1_file);*/

    /****************************************************************************************************************/
    //Segunda recepcion: MD.
    MPI_Recv(md,10000,MPI_CHAR, 0, TAG_ALL, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_CHAR, &bytes_send);
    /****************************************************************************************************************/

    /****************************************************************************************************************/
    //Tercera recepcion: CENTROS ORBITALES
    MPI_Recv(centros,10000,MPI_CHAR, 0, TAG_ALL, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_CHAR, &bytes_send);
    /****************************************************************************************************************/

    /****************************************************************************************************************/
    //Cuarta recepcion: LDIV
    MPI_Recv(ldiv,10000,MPI_CHAR, 0, TAG_ALL, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_CHAR, &bytes_send);
    /****************************************************************************************************************/

    //Invocar a octave para computar lcao.
    functionArguments (0) = md;
    functionArguments (1) = centros;
    functionArguments (2) = sqp1;
    functionArguments (3) = 0;
    functionArguments (4) = my_rank;
    functionArguments (5) = argv[2];
    functionArguments (6) = argv[1];
    functionArguments (7) = argv[3];

    //const octave_value_list result = feval ("lcao", functionArguments, 1);
    result_lcao = feval ("lcao", functionArguments, 1);

    char OK[3] = "OK";
    //float lcao_val = result_lcao(0).float_scalar_value();
    MPI_Send(OK, 1, MPI_UNSIGNED_CHAR, 0, TAG_ALL, MPI_COMM_WORLD);



    printf("Hijo: enviado OK al Master.\n");        
      //Levantar las matrices resultado

      //Hacer el send de las matrices al padre.        
  }

    /*
        int hijo=1;          
        int stat=0;
        void * matrixResult = 
        for (hijo = 1; hijo < size; hijo++){
              
              MPI_Recv(matrixResult, sizeof(matrixResult), MPI_UNSIGNED_CHAR, hijo, 1, MPI_COMM_WORLD, &stat);
              /*Escribo las matrices a disco para que las cargue luego octave en combinar_matrices* /              
              fwrite();
        
              /*En octave combinamos los resultados* /
              const octave_value_list result = feval ("combinar_matrices", functionArguments, 1);          

        }
        const octave_value_list result = feval ("agua_finalizacion", functionArguments, 1);              
        std::cout << "Energia es " << result (0).scalar_value () << std::endl;
        std::cout << "Autovalor es\n" << result (1).matrix_value () << std::endl;
     }
     else {
        /*Recibir del master las sub-matrices* /
        MPI_Recv(&count,1,MPI_INT,source,mtype,MPI_COMM_WORLD,&status);

        /*Invocar al resto de agua para que compute los pares...* /
        const octave_value_list result = feval ("lcao", functionArguments, 1);

        MPI_Send(greeting, strlen(greeting)+1, MPI_BYTE, 0,1,MPI_COMM_WORLD);
     }
 
    */
  
    // MPI_Barrier(MPI_COMM_WORLD);
  
  MPI_Finalize();  /* EXIT MPI */

  //void clean_up_and_exit (int retval, bool safe_to_return);
  clean_up_and_exit(0,1);

  return 0;
}
