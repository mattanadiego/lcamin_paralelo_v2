function [sqp1]=sqp(cmd)
a1=[1:1:cmd];
ra1=size(a1,2);
e=ones(ra1,1);
b=[];
for i=1:ra1
	b=[b;[i*e a1']];
end
e1=find(b(:,1)<=b(:,2));
sqp1=b(e1,:);
return
