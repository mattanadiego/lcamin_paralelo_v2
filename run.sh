#!/bin/bash

NUM_PROC=32
#NUM_PROC2=${NUM_PROC} - 1
MAIN_FILE='lcaomain'
MOLECULE='magua_7'
SEED=10
#ARCHIVO_SALIDA=$MOLECULE"_"$NUM_PROC"procs_time_proc"
COMANDO_1=''

make;
make clean_mat;
make clean_out;
for (( i=1; i<=$SEED ; i++))
do
  mpirun -np $NUM_PROC -machinefile machines_file.txt ./$MAIN_FILE $MOLECULE $i $NUM_PROC
done

#for ((j=1; j<$NUM_PROC ; j++))
#do
#  $COMANDO_1=" "$COMANDO_1" "$ARCHIVO_SALIDA$j
#done
#$COMANDO_1=" "$COMANDO_1" > "$ARCHIVO_SALIDA"1-"$NUM_PROC2

#echo $COMANDO_1

#cd agua/
#cat $COMANDO_1
#cd ..

exit;
