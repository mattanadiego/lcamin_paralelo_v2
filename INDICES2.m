function [vec] =INDICES2(fin)
i=1;
c=1;
d=1;
vec=[];
while c<=fin
    for a=c:fin
        for b=a:fin
            if b>=d || a>c
                %disp([c d a b ]);
                r=[c d a b ];
                vec=[vec; r];
            end
            
        end
    end
    d=d+1;

    if d>fin
        c=c+1;
        d=c;
    end
end

return

