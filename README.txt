==== 25/08/11
Corrimos la versión en Octave, corrigiendo los errores de warnings (el nombre de las funciones en el perfil no coincidía con el nombre del documento donde se almacena la función). Nos dimos cuenta que al no dar los warnings no mostraba por pantalla el resultado de la ejecución. Guillermo sospechó que se desviaba la salida estandar. Buscamos en el manual de GNU octave página 549 Apéndice E: Known Causes of Trouble y figura un bug con respecto a la salida standard.
La solución es usar fflush(stdout) cada vez que se desea mostrar por pantalla.
Se envió el código zipeado a todos los integrantes.
Firmamos las planillas de alta en el proyecto.

