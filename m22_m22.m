% Calculo de cantidades de 4 indices(mod2-mod2|mod2-mod2)
function [fxr]=m22_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
c1=((alfa1*alfa2)/(alfa1+alfa2))*((a1-a2)'*(a1-a2));
c2=((beta1*beta2)/(beta1+beta2))*((b1-b2)'*(b1-b2));
den=sqrt(pi)*(alfa1+alfa2)*(beta1+beta2)*...
  sqrt(alfa1+alfa2+beta1+beta2);
num=16*(alfa1*alfa2*beta1*beta2)^(.75)*exp(-c1-c2);
cte=num/den;
p12=(1/(alfa1+alfa2))*(alfa1*a1+alfa2*a2);
p34=(1/(beta1+beta2))*(beta1*b1+beta2*b2);   
p1234=norm(p12-p34);  
c3=((alfa1+alfa2)*(beta1+beta2))/(alfa1+alfa2+beta1+beta2);
x0=sqrt(c3)*p1234;
if x0
   fxr=0.5*(sqrt(pi)/x0)*erf(x0);
else
   fxr=1;
end
fxr=cte*fxr;
return  

