%Calculo de las matrices V y T en forma analitica
function [vta,vsa,vva]=comp_v(md,centros,ldiv,sqp1,...
   vta,vsa,vva,mumodif)
if nargin==4
  mumodif=[];
end

if isempty(mumodif)
   vta=[];
   vsa=[];
   vha=[];
   vva=[];
   rsqp1=size(sqp1,1);
else
   sqp10=sqp1;
   %[rsqp1,sqp1]=calc_red(mumodif,sqp1);
end

rr1 = centros;
zhh=centros(4,:);


if ldiv
   ldiv0 = ldiv;
else
   ldiv0=0;
end
clc

%tic
for par1=1:rsqp1
   %C�lculo de T
   va=0;
   a1=md(1:3,sqp1(par1,1)) ;
   b1=md(1:3,sqp1(par1,2)) ;
   alfa1=md(4,sqp1(par1,1));
   beta1=md(4,sqp1(par1,2));
   ctrlcp1 = control2(sqp1,par1,ldiv0,a1,b1);
   ta= calc_t(a1,alfa1,b1,beta1,ctrlcp1);
   %vta=[vta;ta];
   %C�lculo de S
   sa= calc_s(a1,alfa1,b1,beta1,ctrlcp1);
   %vsa=[vsa;sa];
   %C�lculo de V
   for j=1:size(centros,2)
      r = rr1(1:3,j);
      va0=calc_v(a1,alfa1,b1,beta1,r,par1,ctrlcp1);
      va=va + va0 * zhh(j);
   end
   if isempty(mumodif)
      vta=[vta;ta];
      vsa=[vsa;sa];
      vva=[vva;va];
   else
      munu=find((sqp10(:,1)==sqp1(par1,1))&&...
         (sqp10(:,2)==sqp1(par1,2)));
      vta(munu)=ta;
      vsa(munu)=sa;
      vva(munu)=va;
   end
end
%toc
return   

% ================================================= 
function [va]=calc_v(a1,alfa1,b1,beta1,r,par1,ctrlcp1)
switch ctrlcp1
case 1
   va=cv_1(a1,alfa1,b1,beta1,r);  				% (1sa|Z/ra|1sa), (1sa|Z/rb|1sa)
																		% (1sa|Z/ra|1sa), (1sa|Z/rb|1sa')
case 2
   va=cv_2(a1,alfa1,b1,beta1,r,par1);  		% (1sa|Z/ra|1sb)
case 3
   va=cv_3(a1,alfa1,b1,beta1,r);   		 		% (1sa|Z/ra|1gb)
case 4
   va=cv_4(a1,alfa1,b1,beta1,r,ctrlcp1);  	% (1ga|Z/ra|1ga'),(1ga|Z/ra|1gb)
end
return

% ================================================= 
function [va]= cv_1(a1,alfa1,b1,beta1,r)
pseda=0.5*(alfa1+beta1);
tau=(alfa1-beta1)/(alfa1+beta1);
if ~any(a1-r)
   va=sqrt((1-tau^2)^3)*pseda;
else
   ro=pseda*norm(a1-r);
   va=sqrt((1-tau^2)^3)*(pseda/ro)*(1-(1+ro)*exp(-2*ro));
end
return

% ================================================= 
function [va]= cv_2(a1,alfa1,b1,beta1,r,par1)
global lxv2 lyv2 lzv2 puntosv2
norma=norm(a1-b1);
pseda=0.5 * (alfa1 + beta1);
ro= pseda*norma;
tau= (alfa1-beta1)/(alfa1 + beta1);
roa=alfa1*norma;
rob=beta1*norma;
if (~any(a1-r))||(~any(b1-r))
   if (alfa1 == beta1) 		% Tau=0
      va= pseda*(1+ro)*exp(-ro);
   else;
      kapa= (alfa1^2+beta1^2)/(alfa1^2-beta1^2);
      tau= (alfa1-beta1)/(alfa1+beta1);
      if ~any(a1-r) 
         va= pseda*(1+tau)*sqrt(1-tau^2)/(tau*ro);
         va= va*((kapa-1)*exp(-roa)+(1-kapa+rob)*exp(-rob));
      else % r=b1, tau=-tau
         va= -pseda*(1-tau)*sqrt(1-tau^2)/(tau*ro);
         va=va*((-kapa-1)*exp(-rob)+(1+kapa+roa)*exp(-roa));
      end
   end
else
    nmu= alfa1^1.5/pi^.5;
    nnum= beta1^1.5/pi^.5;
    [ireal] = intquad3('fcv_2',lxv2,lyv2,lzv2,puntosv2,a1,alfa1,b1,beta1,r);
    va= ireal*nmu*nnum;
end
return


% ================================================= 
function [va]=cv_3(a1,alfa1,b1,beta1,r)
global liv3 lsv3 puntosv3
cte=((alfa1^3/pi)^.5)*(2*beta1/pi)^.75*(2/pi^.5);
k1=((alfa1^3/pi)^.5)*(2*beta1/pi)^.75;
k1=k1*2*pi^.5*alfa1^(-1)*beta1^(-.5);
lsv3=(log(k1*10^8))^.5;
[va,iimag]=intquad1('fcv_3',liv3,lsv3,puntosv3,a1,alfa1,b1,beta1,r);
va=cte*va;
return

% ================================================= 
function [va]= cv_4(a1,alfa1,b1,beta1,r,ctrlcp1)
global liv4 lsv4 puntosv4
a0=alfa1+beta1;
p=(alfa1*a1+beta1*b1)/a0;
norma=(p-r)'*(p-r);
s=calc_s(a1,alfa1,b1,beta1,ctrlcp1);
c1= 2*(a0/pi)^0.5;
if norma
   [ireal,iimag]=intquad1('fcv_4',liv4,lsv4,puntosv4,alfa1,beta1,norma);
   va= c1 * s * ireal;
else
   va= c1*s;
end
return     

% ================================================= 
function [va]= cv_2old(a1,alfa1,b1,beta1,r,par1)
global lxv2 lyv2 lzv2 puntosv2
norma=norm(a1-b1);
pseda=0.5 * (alfa1 + beta1);
ro= pseda*norma;
ro1= 0.5 * (alfa1 + beta1) * norma;
prom=(alfa1+beta1)*.5;
tau= (alfa1-beta1)/(alfa1 + beta1);
rou=alfa1 * norma;
ron=beta1 * norma;
if all(a1==r) || all(b1==r)
   if alfa1 == beta1
      va= alfa1 * (1+ro1) * exp(-ro1);
   else;
      if all(a1==r)
         ctek= (alfa1^2 + beta1^2) / (alfa1^2 - beta1^2);
         va= prom * (1+tau) * ((1-tau^2)^0.5 / (tau*ro1));
         va= va*((ctek-1)*exp(-rou)+(1-ctek+ron)*exp(-ron));
      else
         c=alfa1;
         alfa1=beta1;
         beta1=c;
         ctek= (alfa1^2 + beta1^2) / (alfa1^2 - beta1^2);
         tau= (alfa1-beta1)/(alfa1 + beta1);
         va= prom * (1+tau) * ((1-tau^2)^0.5 / (tau*ro1));
         va=va*((ctek-1)*exp(-ron)+(1-ctek+rou)*exp(-rou));
      end
   end
else
    nmu= alfa1^1.5/pi^.5;
    nnum= beta1^1.5/pi^.5;
    [ireal] = intquad3('fcv_2',lxv2,lyv2,lzv2,puntosv2,a1,alfa1,b1,beta1,r);
    va= ireal*nmu*nnum;
end
return


% ================================================= 
% Rutinas para el calculo de la matriz T en forma anal�tica. 
function [ta]=calc_t(a1,alfa1,b1,beta1,ctrlcp1);
switch ctrlcp1
case 1
   ta= ct_1(a1,alfa1,b1,beta1); 	   % (1sa|1sa),(1sa|1sa')
case 2
   ta= ct_2(a1,alfa1,b1,beta1,ctrlcp1); 		% (1sa|1sb)
case 3
   ta= ct_3(a1,alfa1,b1,beta1,ctrlcp1); 		% (1sa|1ga) o (1sa|1gb)
case 4
   ta= ct_4(a1,alfa1,b1,beta1); 		% (1ga|1ga),(1ga|1ga'),(1ga|1gb) 
end
return


% ================================================= 
function [ta]=ct_1(a1,alfa1,b1,beta1);
pseda=0.5*(alfa1+beta1);
tau=(alfa1-beta1)/(alfa1+beta1);
ov01=sqrt(0.5*(1+tau)*(1-tau)^3);
ov11=sqrt(((1+tau)*(1-tau))^3);
ta=-0.5*pseda^2*(1+tau)^2*(ov11-2*sqrt(2)*ov01);
return

% ================================================= 
function [ta]=ct_2(a1,alfa1,b1,beta1,ctrlcp1)
norma=norm(a1-b1);
pseda= 0.5 * (alfa1 + beta1);
ro=pseda* norma;
tau= (alfa1-beta1)/(alfa1 + beta1);
ra=alfa1 * norma;
rb=beta1 * norma;
if (alfa1-beta1)
   s1=sqrt(1-tau^2)/(tau*ro);
   kapa= (alfa1^2 + beta1^2) / (alfa1^2 - beta1^2);
   ov11=calc_s(a1,alfa1,b1,beta1,ctrlcp1);
   ov01= (s1/sqrt(2))*((kapa-1)*exp(-ra)+(1-kapa+rb)*exp(-rb));
   ta=-(1/2)*pseda^2*(1+tau)^2*(ov11-2*sqrt(2)*ov01);
else
   ta=(alfa1^2)*exp(-ro)*(1/2+ro/2-(ro^2)/6);
end
return

% ================================================= 
function [ta]=ct_3(a1,alfa1,b1,beta1,ctrlcp1)
global puntost3 lit3 lst3
norma2=(a1-b1)'*(a1-b1);
k1=2^(11/4)*pi^(-.25)*alfa1^.5*beta1^(-.25)*3^(-1.5);
lst3=(log(10^8*k1))^.5;
[ireal,iimag] = intquad1('fct_3',lit3,lst3,puntost3,...
   a1,alfa1,b1,beta1,norma2);   
s=calc_s(a1,alfa1,b1,beta1,ctrlcp1);
ctet=alfa1^1.5 * (beta1^(11/4)) * (2^(23/4)) / (pi^.25);
ta=3*beta1*s-ctet*ireal;
return

% ================================================= 
function [ta]=ct_4(a1,alfa1,b1,beta1);
norma2=(a1-b1)'*(a1-b1);
ta=((4*alfa1*beta1)^(0.75))/((alfa1+beta1)^(3.5));
ta=ta*exp(-(alfa1*beta1)*norma2/(alfa1+beta1));
ta=ta*(3*alfa1*beta1*(alfa1+beta1)-2*norma2*(alfa1*beta1)^2);
return


