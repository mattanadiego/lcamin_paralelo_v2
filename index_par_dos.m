%Devuelve el indice en el cual comienzan las funciones par dos.
function [index]=index_par_dos(sqp1)

    c=find(sqp1(:,1)>=4);
    index=c(1);

return
