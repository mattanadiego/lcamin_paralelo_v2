% *************************************************************
% 					ENCABEZAMIENTO. NO MODIFICAR
% *************************************************************
clear all
addpath('/home/laura/Documentos/investigacion/lcaomin_jor','/home/laura/Documentos/investigacion/lcaomin_jor/ejemplos')
global mdatos mum objetivo
globales

% *************************************************************
% 					DATOS A MODIFICAR CON CADA EJEMPLO

objetivo='agua';

% *************************************************************

mdatos=strcat('M',objetivo);
pmdatos=strcat('P',mdatos);
[param0,mum,chgd,indp,corrida]=feval(pmdatos);
fname=strcat('h',mdatos,'_',corrida);

% *************************************************************
% 					SELECCION DEL METODO DE OPTIMIZACION
% select = 1       Metodos quasi Newton
% select = 2		 Metodo SImplex de Nelder
% *************************************************************
select = 1;

%options=optimset('GradObj','on');

% *************************************************************
% 				RUTINAS DE OPTIMIZACION. NO MODIFICAR
% *************************************************************
switch select
case 1
   tol=1e-6;
   TolFun=1e-6;
maxit=10;


   disp('tiempo1')
   tiempo1=fix(clock)
   
   [param,energia,historia,ctrlend,costdata] = optimizar(objetivo,...
      param0,mum,tol,chgd,TolFun,maxit,indp);
   
 	save(fname,'historia','ctrlend','costdata') 
case 2
   [param,energia,flags,salidas]=fminsearch(objetivo,param0);
otherwise
   [param,energia,flags,salidas]=fminsearch(objetivo,param0);
end
   
 
