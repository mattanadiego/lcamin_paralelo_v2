function [b]=sqpm(fmd)
	a1=[1:1:fmd]';
   e=ones(fmd,1);
   b=[];
   for i=1:fmd
      b=[b;[i*e a1]];
   end
return   

